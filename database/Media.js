var  connections  = require("./index");
var  mongoose = require('mongoose');

var connector    =   connections['default'];

const MediaSchema=new mongoose.Schema({
    name:{
        type:String
    },
    mime:{
        type:String
    },
    path:{
        type:String
    },
    createdAt:{
        type:Date
    },
    updatedAt:{
        type:Date
    }
});

const Media = connector.connection.model('Media',MediaSchema);

Media.connection   =   connector.connection;
Media.connection_secret   =   connector.secret;


module.exports =  Media;
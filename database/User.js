var  connections  = require("./index");
var  mongoose = require('mongoose');

var connector    =   connections['default'];

const UserSchema=new mongoose.Schema({
    name:{
        type:String
    },
    email:{
        type:String
    },
    password:{
        type:String
    },
    phone_number:{
        type:String
    },
    user_type:{
        type:Number
    },
    status:{
        type:Boolean
    },
    last_login_at:{
        type:Date
    },
    createdAt:{
        type:Date
    },
    updatedAt:{
        type:Date
    }
});

const User = connector.connection.model('Users',UserSchema);

User.connection   =   connector.connection;
User.connection_secret   =   connector.secret;


module.exports =  User;
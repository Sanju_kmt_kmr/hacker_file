var  connections  = require("./index");
var  mongoose = require('mongoose');

var connector    =   connections['default'];


const UserExpenseSchema=new mongoose.Schema({
    media_id:{ 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Media' 
    },
    name:{
        type:String
    },
    email:{
        type:String
    },
    phone:{
        type:String
    },
    amount:{
        type:String
    },
    preferred_type:{
        type:String
    },
    members:{
        type:String
    },
    createdAt:{
        type:Date
    },
    updatedAt:{
        type:Date
    }
});

const UserExpense = connector.connection.model('UserExpense',UserExpenseSchema);

UserExpense.connection   =   connector.connection;
UserExpense.connection_secret   =   connector.secret;


module.exports =  UserExpense;
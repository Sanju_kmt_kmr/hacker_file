/*
 |  This is the file used to setup
 | the schema of application
 |
 */

var  mongoose = require('mongoose');
var  mongo_config  = require('./../config/mongo.json');

// var mongo_config    =   mongo['default'];

// const   url     =   "mongodb://"+mongo_config['host']+":"+mongo_config['port']+"/"+mongo_config['db_name'];
// mongoose.connect(url);

// mongoose.connection.on('connected',()=>{
//     console.log('connect to database  '+url);
// });

// mongoose.connection.on('error',(err)=>{
//     console.log('Database error:'+err);
// });

var connections     =   {};

var connection_names    =   Object.keys(mongo_config);
connection_names.forEach((connection_name)=>{
    var connection =    null;
    var secret      =   null;
    var connection_config   =   mongo_config[connection_name];
    if(connection_config != undefined){
        var host                =   connection_config['host'];  
        var db_name                =   connection_config['db_name'];  
        var port                =   connection_config['port'];  
        secret              =   connection_config['secret'];
        var username              =   connection_config['username'];
        var password              =   connection_config['password'];
        if(username != undefined && username.length > 0 && password != undefined && password.length > 0 ){
            connection     = mongoose.createConnection('mongodb://'+username+':'+password+'@'+host+':'+port+'/'+db_name);
            console.log('mongodb://'+username+':'+password+'@'+host+':'+port+'/'+db_name);
        } else {
            connection     = mongoose.createConnection('mongodb://'+host+':'+port+'/'+db_name);
        }
        
        console.log("connection created");
        connection['secret']    =   secret;
    }

    connections[connection_name]    =   {
        connection:connection
    };
})

module.exports = connections; 
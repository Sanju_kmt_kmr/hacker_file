
import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import { Switch, Route,Redirect }    from 'react-router-dom';

import Login  from './Login/index.jsx';
import UserList  from './Application/User/List/index.jsx';
import AddUser  from './Application/User/Add/index.jsx';
import UserProfileGeneral  from './Application/User/Update/General/index.jsx';

import Blank  from './Blank/index.jsx';

import {me as CurrentUser} from "../actions/useraction";

import "./app.scss";

class App extends React.Component {
    constructor(props){
        super(props);
        this.state  =   {
            'is_logged_in':false
        }
    }

    render() {
        return (
            <Switch>
                <Route exact path='/' component={Blank}/>
                <Route exact path='/login' component={Login}/>  
                <Route exact path='/app/admin-user/list' component={UserList}/>
                <Route exact path='/app/admin-user/add' component={AddUser}/>
                <Route exact path='/app/admin-user-profile/:id' component={UserProfileGeneral}/>


                <Redirect from="/app" to="/app/admin-user/list"/>
            </Switch>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(App))
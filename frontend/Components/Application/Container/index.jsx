import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';

import Header from "./../../Common/Header/index.jsx";
import Sidenav from "./../../Common/Sidenav/index.jsx";
import Footer from "./../../Common/Footer/index.jsx";
import {me as CurrentUser,me_thunk as CurrentUserThunk} from "../../../actions/useraction";


import "./style.scss";
import AlertPopup from "./../../Common/Modals/AlertRedux.jsx";

class Container extends React.Component {
    componentWillMount(){
        var me         =    this;
        CurrentUser().then(()=>{
            me.props.dispatch(CurrentUserThunk());    
        }).catch(error=>{
            me.props.history.push("/login");
        });
    }
    
    componentDidMount(){
        $('body').attr('class','hold-transition skin-purple sidebar-min');
    }

    render() {
        var screen_height   =    Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - 100;
        return (
            <div className='wrapper'>
                <Header />
                <Sidenav nav_name={this.props.nav_name}/>
                <div className="content-wrapper" style={{"minHeight":screen_height+"px"}}>
                    { this.props.children }
                </div>
                <Footer />
                <AlertPopup/>
            </div>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(Container))
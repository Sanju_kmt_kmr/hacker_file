import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {Button,Alert} from 'react-bootstrap';
import MyProfileContainer from "../container.jsx";

import {me as CurrentUser,changePassword } from "../../../../actions/useraction";

class MyProfilePassword extends React.Component {

    constructor(props){
        super(props);
        this.state  =       {
            '_id':'',
            'current_password':'',
            'current_password_error':'',
            'new_password':'',
            'new_password_error':'',
            'confirm_password':'',
            'confirm_password_error':'',
            'success':'',
            'error':'',
            'inProg':false
        };
        this.init   =   this.init.bind(this);
        this.update     =   this.update.bind(this);
    }

    componentWillMount(){
        var me         =    this;
        CurrentUser().then((user)=>{
            console.log(user);
            me.setState({
                'id':user._id,
            });
        });
    }

    componentDidMount(){
        // document.getElementById('input_current_password').focus();
    }

    
    init(){
        this.setState({
            'current_password':'',
            'current_password_error':'',
            'new_password':'',
            'new_password_error':'',
            'confirm_password':'',
            'confirm_password_error':'',
            'success':'',
            'error':''
        });
    }

    update(){

        this.setState({'current_password_error':'','new_password_error':'','confirm_password_error':'','success':'','error':''});

        var current_password       =       this.state.current_password;
        if(current_password == undefined  || current_password == null || current_password.length == 0){
            this.setState({'current_password_error':"Please provide current password of your profile."})
            document.getElementById('input_current_password').focus();
            return;
        }   

        var new_password       =       this.state.new_password;
        if(new_password == undefined  || new_password == null || new_password.length == 0){
            this.setState({'new_password_error':"Please provide new password of your profile."})
            document.getElementById('input_new_password').focus();
            return;
        }
        
        if(new_password.trim().length < 0){
            this.setState({'new_password_error':"Password should be of minimum 6 characters."})
            document.getElementById('input_new_password').focus();
            return;
        }

        if(current_password == new_password){
            this.setState({'new_password_error':"New password value is same as current password entered by you."});
            document.getElementById('input_new_password').focus();
            return;
        }

        var confirm_password       =       this.state.confirm_password;
        if(new_password != confirm_password){
            this.setState({'confirm_password_error':"Confirm password does not match with new password you have provided."})
            document.getElementById('input_confirm_password').focus();
            return;
        }

        var me          =       this;
        me.setState({'inProg':true});
        changePassword(me.state.id,current_password,new_password).then(()=>{
            me.setState({'success':'Profile password updated successfully.'});
            setTimeout(()=>{
                me.setState({'success':''});
            },10000)
            document.getElementById('input_current_password').focus();
            me.setState({
                'current_password':'',
                'new_password':'',
                'confirm_password':'',
            });
            me.setState({'inProg':false});
            CurrentUser();
        }).catch(error=>{
            if(error.error != undefined){
                me.setState({'error':error.error});
            } else {
                console.log(error);
                me.setState({'error':"Something went wrong"});
            }
            me.setState({'inProg':false});
        });
    }

    render() {

        var state   =   this.state;

        if(state.id == null){
            return null;
        }

        var current_password_input_class   =   "form-group  ";
        if(state.current_password_error.length != 0){
            current_password_input_class   = "form-group  error_input";
        }

        var new_password_input_class   =   "form-group  ";
        if(state.new_password_error.length != 0){
            new_password_input_class   = "form-group  error_input";
        }

        var confirm_password_input_class   =   "form-group  ";
        if(state.confirm_password_error.length != 0){
            confirm_password_input_class   = "form-group  error_input";
        }


        return (
            <MyProfileContainer page={'changepassword'}>
                <div className="box box-success">
                    <div className="box-header with-border">
                        <h3 className="box-title">Change Password</h3>
                    </div>
                    <div className="box-body">
                    <div className='row'>
                            <div className='col-lg-3'></div>
                            <div className='col-lg-6'>
                                <div className='form'>
                                    {state.error.length != 0 && <Alert bsStyle="danger" >{state.error}</Alert>}
                                    {state.success.length != 0 && <Alert bsStyle="success" >{state.success}</Alert>}
                                    <div className={current_password_input_class}>
                                        <label>Current Password</label>
                                        <input className="form-control" id='input_current_password' type='password' value={state.current_password} tabIndex={1} placeholder="Current Password" onChange={(event)=>{this.setState({'current_password':event.target.value})}}/>
                                        {state.current_password_error.length != 0 && <div className='error'>{state.current_password_error}</div>}
                                    </div>
                                    <div className={new_password_input_class}>
                                        <label>New Password</label>
                                        <input className="form-control" id='input_new_password' type='password' value={state.new_password} tabIndex={2} placeholder="New Password" onChange={(event)=>{this.setState({'new_password':event.target.value})}}/>
                                        {state.new_password_error.length != 0 && <div className='error'>{state.new_password_error}</div>}
                                    </div>
                                    <div className={confirm_password_input_class}>
                                        <label>Confirm Password</label>
                                        <input className="form-control" id='input_confirm_password' type='password' value={state.confirm_password} tabIndex={3} placeholder="Confirm Password" onChange={(event)=>{this.setState({'confirm_password':event.target.value})}}/>
                                        {state.confirm_password_error.length != 0 && <div className='error'>{state.confirm_password_error}</div>}
                                    </div>
                                    <hr/>
                                    <div className='text-right'>
                                    {!state.inProg && <Button bsStyle='default' className='bg-purple' tabIndex={4} onClick={this.update}>Update</Button>}
                                        {state.inProg && <Button bsStyle='default' className='bg-purple' tabIndex={4} ><i className="fa fa-spinner fa-spin" style={{fontSize:"20px"}}></i></Button>}
                                        &nbsp;&nbsp;
                                        <Button tabIndex={5} onClick={this.init}>Cancel</Button>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-3'></div>
                        </div>
                    </div>
                </div>
            </MyProfileContainer>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(MyProfilePassword))
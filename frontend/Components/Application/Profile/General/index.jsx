import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {Button,Alert} from 'react-bootstrap';
import {me as CurrentUser,me_thunk as CurrentUserThunk,update as updateUser} from "../../../../actions/useraction";

import MyProfileContainer from "../container.jsx";

class MyProfileGeneral extends React.Component {
    constructor(props){
        super(props);
        this.state  =   {
            'id':'',
            'name':'',
            'name_error':'',
            'email':'',
            'error':'',
            'success':'',
            'inProg':false
        };
        this.init   =   this.init.bind(this);
        this.update   =   this.update.bind(this);
    }

    componentWillMount(){
            this.init();
    }
    
    init(){
        var me         =    this;
        CurrentUser(true).then((user)=>{
            me.setState({
                '_id':user._id,
                'name':user.name,
                'email':user.email
            });
        });
    }

    componentDidMount(){
        document.getElementById('profile_name').focus();
    }

    update(){
        this.setState({'error':'','name_error':'','success':''});
        var name       =       this.state.name;
        if(name == undefined  || name == null || name.trim().length == 0){
            this.setState({'name_error':"Please provide name."})
            document.getElementById('profile_name').focus();
            return;
        }   

        var me          =       this;
        me.setState({'inProg':true});
        console.log(me.state._id);
        updateUser(me.state._id,{'name':name}).then(()=>{
            me.setState({'success':'Profile updated successfully.'});
            setTimeout(()=>{
                me.setState({'success':''});
            },10000)
            me.setState({'inProg':false});
            document.getElementById('profile_name').focus();
            this.props.dispatch(CurrentUserThunk());
        }).catch(error=>{
            if(error.error != undefined){
                me.setState({'error':error.error});
            } else {
                console.log(error);
                me.setState({'error':"Something went wrong"});
            }
            me.setState({'inProg':false});
        });
    }

    render() {
        var state   =   this.state;
        if(state.id == null){
            return null;
        }

        var name_input_class   =   "form-group  ";
        if(state.name_error.length != 0){
            name_input_class   = "form-group  error_input";
        }


        return (
            <MyProfileContainer page={'myprofile'}>
                <div className="box box-success">
                    <div className="box-header with-border">
                        <h3 className="box-title">General Profile</h3>
                    </div>
                    <div className="box-body">
                        <div className='row'>
                            <div className='col-lg-1'></div>
                            <div className='col-lg-3 text-center'>
                                <img src="/images/default_user.png" style={{'width':'80%'}}/>
                            </div>
                            
                            <div className='col-lg-6'>
                                <div className='form update-profile'>
                                    {state.error.length != 0 && <Alert bsStyle="danger" >{state.error}</Alert>}
                                    {state.success.length != 0 && <Alert bsStyle="success" >{state.success}</Alert>}
                                    <div className={name_input_class}>
                                        <label>Name</label>
                                        <input className="form-control" id='profile_name' type='text' tabIndex={1} placeholder="Name" value={state.name} onChange={(event)=>{this.setState({'name':event.target.value})}}/>
                                        {state.name_error.length != 0 && <div className='error'>{state.name_error}</div>}
                                    </div>
                                    <div className='form-group'>
                                        <label>Email</label>
                                        <input className="form-control" type='text' placeholder="Email" value={state.email} onChange={(event)=>{}} readOnly/>
                                    </div>
                                    <hr/>
                                    <div className='text-right'>
                                        {!state.inProg && <Button bsStyle='default' className='bg-purple' tabIndex={2} onClick={this.update}>Update</Button>}
                                        {state.inProg && <Button bsStyle='default' className='bg-purple' tabIndex={2} ><i className="fa fa-spinner fa-spin" style={{fontSize:"20px"}}></i></Button>}
                                        &nbsp;&nbsp;
                                        <Button tabIndex={3} onClick={this.init}>Cancel</Button>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-2'></div>
                        </div>
                    </div>
                </div>
            </MyProfileContainer>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(MyProfileGeneral))
import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';

import Container from "../Container/index.jsx";
import MyProfileNav from "./sidenav.jsx";

import "./style.scss";

class MyProfileContainer extends React.Component {
    componentWillMount(){
    }
   
    render() {

        var page   =   this.props.page;
        if(page == undefined){
            page    =   "myprofile";
        }
        return (
            <Container >
                <section className="content">
                    <div className="box box-success">
                        <div className="box-header with-border">
                            <h3 className="box-title">My Profile</h3>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-lg-2'>
                            <MyProfileNav page={page} />
                        </div>
                        <div className='col-lg-10'>
                            { this.props.children}
                        </div>
                    </div>
                </section>
            </Container>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(MyProfileContainer))
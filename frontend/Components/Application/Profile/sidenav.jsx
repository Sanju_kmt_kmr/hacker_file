import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import { Nav,NavItem} from 'react-bootstrap';

import Container from "../Container/index.jsx";

class MyProfileNav extends React.Component {
    constructor(props){
        super(props);
        this.handleSelect       =   this.handleSelect.bind(this);
    }
    componentWillMount(){
    }
    
    handleSelect(){

    }
    render() {
        var page   =   this.props.page;
        if(page == undefined){
            page    =   "myprofile";
        }

        var activeKey   =   1;
        if(page == 'myprofile'){
            activeKey   =   1;
        } else {
            activeKey   =   2;
        }
        return (
            <div className="box box-success">
                <div className="box-body">
                    <Nav bsStyle="pills" stacked activeKey={activeKey} onSelect={this.handleSelect}>
                        <NavItem eventKey={1} href="#/app/myprofile">
                            General Profile
                        </NavItem>
                        <NavItem eventKey={2} href="#/app/myprofile/password">
                            Change Password
                        </NavItem>
                    </Nav>
                </div>
            </div>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(MyProfileNav))
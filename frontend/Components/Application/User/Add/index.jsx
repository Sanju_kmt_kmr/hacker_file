import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {Button,Alert,FormControl} from 'react-bootstrap';
import {me_thunk as CurrentUserThunk,add as addUser} from "../../../../actions/useraction";
import * as Validate from "../../../../actions/validation";
import {show_popup} from "./../../../../actions/alertpopupaction";
import {upload} from "../../../../actions/mediaaction";


import Container from "../../Container/index.jsx";

class AddUser extends React.Component {
    constructor(props){
        super(props);
        this.state  =   {
            'name':'',
            'name_error':'',
            'email':'',
            'email_error':'',
            'phone':'',
            'phone_error':'',
            'amount':'',
            'amount_error':'',
            'preferred_type':'Individual',
            'members':'',
            'members_error':'',
            'media_id':'',
            'file_name':'',
            'error':'',
            'success':'',
            'inProg':false
        };
       
        this.add   =   this.add.bind(this);
        this.openFileBrowser    =   this.openFileBrowser.bind(this);
        this.uploadDocument     =   this.uploadDocument.bind(this);

    }

    componentWillMount(){
    }
    
    init(){
    }

    componentDidMount(){
        setTimeout(function(){
            document.getElementById('profile_name').focus();
        },100);
    }
    uploadDocument(event){
        this.setState({'upload_error':""});

        var files   =   event.target.files;
        if(files.length > 0){
            var file    =   files[0];
            if(!Validate.validateImage(file)){
                this.setState({'upload_error':'Please upload valid pdf document.'});
                return;
            }

            var me  =   this;
            this.setState({'upload_prog':true});

            var data    =   this.props.data;

            upload(file).then((response)=>{
                me.props.dispatch(show_popup("Information","Document uploaded successfully."));
                this.setState({'media_id':response._id,'upload_prog':false,'file_name':response.name});
                
                if(me.props.onChange != undefined){
                    me.props.onChange();
                }
            }).catch(error=>{
                this.setState({'upload_prog':false,'upload_error':"Failed to upload document"});
            });
        }
    }

    openFileBrowser(event){
        this.imageInput.value = null;
        this.imageInput.click();
    }
   
    add(){
        this.setState({'error':'','name_error':'','email_error':'','phone_error':'','amount_error':'','preferred_type':'','success':''});
        var name       =       this.state.name;
        if(name == undefined  || name == null || name.trim().length == 0){
            this.setState({'name_error':"Please provide name."})
            document.getElementById('profile_name').focus();
            return;
        }   

        var email       =       this.state.email;
        if(email == undefined  || email == null || email.trim().length == 0){
            this.setState({'email_error':"Please provide email address."});
            document.getElementById('profile_email').focus();
            return;
        }   

        if(!Validate.email(email)){
            this.setState({'email_error':"Please provide a valid email address."});
            document.getElementById('profile_email').focus();
            return;
        }

        var phone       =       this.state.phone;
        if(phone == undefined  || phone == null || phone.trim().length < 6){
            this.setState({'phone_error':"Please provide phone with minimum 6 characters."})
            document.getElementById('profile_phone').focus();
            return;
        }   


        var amount       =       this.state.amount;
        if(amount == undefined || amount == null || amount.trim().length == 0){
            this.setState({'amount_error':"Amount is empty."})
            document.getElementById('profile_amount').focus();
            return;
        }
        var prefered_type = this.state.preferred_type;
        console.log(prefered_type);
        if(prefered_type != 'Individual'){
            var members       =       this.state.members;
            if(members == null || members.length <= 0){
                this.setState({'members_error':"Please enter number of members."});
                document.getElementById('profile_members').focus();
                return;
            }
        }
        var media_id    =    this.state.media_id;
        var postData = {
            "name":name,
            "email":email,
            "phone":phone,
            "amount":amount,
            'media_id':media_id,
            "preferred_type":prefered_type,
            "members":members
        }



        var me          =       this;
        me.setState({'inProg':true});
        console.log(postData);
        addUser(postData).then(()=>{
            me.setState({'success':''});
            me.props.dispatch(show_popup("Information","Expense added successfully.",()=>{
                me.props.history.push("/app/admin-user/list");
            }));
            me.setState({'inProg':false});
        }).catch(error=>{
            if(error.error != undefined){
                me.setState({'error':error.error});
            } else {
                console.log(error);
                me.setState({'error':"Something went wrong"});
            }
            me.setState({'inProg':false});
        });
    }

    render() {
        var state   =   this.state;
        

        var name_input_class   =   "form-group  ";
        if(state.name_error.length != 0){
            name_input_class   = "form-group  error_input";
        }

        var email_input_class   =   "form-group  ";
        if(state.email_error.length != 0){
            email_input_class   = "form-group  error_input";
        }

        var phone_input_class   =   "form-group  ";
        if(state.phone_error.length != 0){
            phone_input_class   = "form-group  error_input";
        }

        var amount_input_class   =   "form-group  ";
        if(state.amount_error.length != 0){
            amount_input_class   = "form-group  error_input";
        }
        var member_input_class   =   "form-group";
        if(state.members_error.length != 0){
            member_input_class   = "form-group  error_input";
        }
        var prefered_input_class = 'form-group';

        var member_input_class = 'form-group hide';
        if(state.preferred_type == 'Group'){
            console.log(state.preferred_type);
            member_input_class = 'form-group show';
        }



        return <Container nav_name={'users'}>
                <section className="content">
                    <div className='row'>
                        <div className='col-lg-12'>
                            <div className="box box-success">
                                <div className="box-header with-border">
                                    <h3 className="box-title">Add User Expense</h3>
                                </div>
                                <div className="box-body">
                                    <div className='row'>
                                        <div className='col-lg-1'></div>
                                        <div className='col-lg-6'>
                                            <div className='form update-profile'>
                                                {state.error.length != 0 && <Alert bsStyle="danger" >{state.error}</Alert>}
                                                {state.success.length != 0 && <Alert bsStyle="success" >{state.success}</Alert>}
                                                <div className={name_input_class}>
                                                    <label>Name*</label>
                                                    <input className="form-control" id='profile_name' type='text' tabIndex={1} placeholder="Name" value={state.name} onChange={(event)=>{this.setState({'name':event.target.value})}} tabIndex={1}/>
                                                    {state.name_error.length != 0 && <div className='error'>{state.name_error}</div>}
                                                </div>
                                                <div className={ email_input_class }>
                                                    <label>Email*</label>
                                                    <input className="form-control" id='profile_email' type='text' placeholder="Email" value={state.email} onChange={(event)=>{this.setState({'email':event.target.value})}} tabIndex={2}/>
                                                    {state.email_error.length != 0 && <div className='error'>{state.email_error}</div>}
                                                </div>
                                                <div className={ phone_input_class }>
                                                    <label>Phone*</label>
                                                    <input className="form-control" id='profile_phone' type='text' placeholder="Phone" value={state.phone} onChange={(event)=>{this.setState({'phone':event.target.value})}} tabIndex={3}/>
                                                    {state.phone_error.length != 0 && <div className='error'>{state.phone_error}</div>}
                                                </div>
                                                <div className={ prefered_input_class }>
                                                    <label>Preferred Type*</label>
                                                    <select className="form-control" id='preferred_type' onChange={(event)=>{this.setState({'preferred_type':event.target.value})}} tabIndex={4}>
                                                        <option value="Individual">Individual</option>
                                                        <option value="Group">Group</option>
                                                    </select> 
                                                </div>
                                                <div className={ member_input_class }>
                                                    <label>Members*</label>
                                                    <input className="form-control" id='profile_members' type='text' placeholder="Members" value={state.members} onChange={(event)=>{this.setState({'members':event.target.value})}} tabIndex={5}/>
                                                    {state.members_error.length != 0 && <div className='error'>{state.members_error}</div>}
                                                </div>
                                                <div className={ amount_input_class }>
                                                    <label>Amount*</label>
                                                    <input className="form-control" id='profile_amount' type='text' placeholder="Amount" value={state.amount} onChange={(event)=>{this.setState({'amount':event.target.value})}} tabIndex={6}/>
                                                    {state.amount_error.length != 0 && <div className='error'>{state.amount_error}</div>}
                                                </div>
                                                <hr/>
                                                <div className='text-left'>
                                                    <Button bsStyle='success' onClick={this.openFileBrowser}>
                                                        <i className='fa fa-upload'></i>Upload Document
                                                    </Button>&nbsp;&nbsp;
                                                    {state.file_name}
                                                 </div>
                                                 <input type='file' ref={fileInput => this.imageInput = fileInput} style={{'visibility':'hidden'}}  onChange={this.uploadDocument}/>
                                                <div className='text-right'>
                                                    {!state.inProg && <Button bsStyle='default' className='bg-purple'  tabIndex={6} onClick={this.add}>Add</Button>}
                                                    {state.inProg && <Button bsStyle='default' className='bg-purple' tabIndex={6} ><i className="fa fa-spinner fa-spin" style={{fontSize:"20px"}}></i></Button>}
                                                    &nbsp;&nbsp;
                                                    <Button tabIndex={7} onClick={()=>{this.props.history.goBack    ()}}>Cancel</Button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='col-lg-2'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Container>;
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(AddUser))
import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import {Table,Button,FormControl,ButtonGroup} from 'react-bootstrap';
import Container from "../../Container/index.jsx";
import SearchInput from "../../../Common/SearchInput/index.jsx";
import CPagination from "../../../Common/Pagination/index.jsx";
import {fetchUsers} from "../../../../actions/useraction";
import * as Enviorment from "../../../../actions/enviorment";
import "./style.scss";
import moment from "moment";

class UserList extends React.Component {

    constructor(props){
        super(props);
        this.state      =   {
            'users':[],
            'total_count':0,
            'filter':{
                'search_query':'',
                'page':1,
                'page_size':Enviorment.get('DEFAULT_PAGE_SIZE'),
                // 'page_size':1,
                'order':{
                    'name':1
                }
            },
            'in_prog':false
        };
        this.fetch  =   this.fetch.bind(this);
        this.changePage     =   this.changePage.bind(this);
    }

    componentWillMount(){

    }
    
    componentDidMount(){
        this.fetch();
    }

    changePage(page){
        this.state.filter.page  =   page;
        this.setState({"filter":this.state.filter});
        this.fetch();
    }
    fetch(){
        this.setState({'in_prog':true});
        fetchUsers(this.state.filter).then((result)=>{
            console.log(result);
            var users_data  =   result.data;
            var items   =   users_data.rows;
            this.setState({'users':items,'total_count':users_data.count,'in_prog':false});
        }).catch(error=>{
            this.setState({'users':[],'in_prog':false});
        });
    }

    render() {        
        var state   =   this.state;
        var total_page  =   Math.ceil(this.state.total_count/this.state.filter.page_size);
        console.log(total_page);
        var page_start_index    =   ((state.filter.page-1)*state.filter.page_size);
        var me          =   this;
        var     sort_options    =   [
            {
                'name':'Name - ascending',
                'value':"name#asc"
            },
            {
                'name':'Name - descending',
                'value':"name#desc"
            },
            {
                'name':'Email - ascending',
                'value':"email#asc"
            },
            {
                'name':'Email - descending',
                'value':"email#desc"
            },
            {
                'name':'Last Login At - ascending',
                'value':"last_login_at#asc"
            },
            {
                'name':'Last Login At - descending',
                'value':"last_login_at#desc"
            },

            {
                'name':'Created At - ascending',
                'value':"createdAt#asc"
            },
            {
                'name':'Created At - descending',
                'value':"createdAt#desc"
            }
        ];


        var sort_value  =   null;
        var keys    =   Object.keys(state.filter.order);
        if(keys[0] != undefined){
            sort_value  =   keys[0]+"#"+(state.filter.order[keys[0]]==1?"asc":'desc');
        }

        return (
            <Container nav_name={'users'}>
                <section className="content">
                    <div className="box box-success">
                        <div className="box-header with-border">
                            <h3 className="box-title">Admin User List</h3>
                            <Button bsStyle='default' className='pull-right bg-purple' onClick={()=>{this.props.history.push('/app/admin-user/add')}}> <i className='fa fa-plus'></i> Add User</Button>
                        </div>
                        <div className="box-body">
                            <div className='row'>
                                <div className='col-lg-3'>
                                    <div className="form-group">
                                        <label>Sort By</label>
                                        <FormControl   onChange={(event)=>{
                                                        var value   =   event.target.value;
                                                        var value_split     =   value.split("#");
                                                        state.filter.order  =  {};
                                                        state.filter.order[value_split[0]] = value_split[1]=="asc"?1:-1;
                                                        this.setState({'fiter':state.filter});
                                                        setTimeout(function(){me.fetch()});
                                                    }
                                                } value={sort_value} componentClass="select" tabIndex={5}>
                                            { 
                                                sort_options.map((option,i)=>{
                                                    return <option value={option.value} key={i}>{option.name}</option>
                                                })
                                            }
                                        </FormControl>
                                    </div>
                                </div>
                                <div className='col-lg-6'></div>
                                <div className='col-lg-3'>
                                    <div className="form-group">
                                        <label>&nbsp;</label>
                                        <SearchInput placeholder={'Search User'} value={state.filter.search_query} onSubmit={(value)=>{ state.filter.page = 1;this.setState({'filter':state.filter});this.fetch();}} onChange={(value)=>{state.filter.search_query = value;this.setState({'filter':state.filter});}}/> 
                                    </div>
                                </div>
                            </div>
                            <div className='userlisttable'>
                                <Table striped bordered condensed hover className='app_table'>
                                    <thead>
                                        <tr>
                                            <th className='text-center clmSl'>Sl.</th>
                                            <th className='clmName'>Name</th>
                                            <th className='clmEmail'>Email Address</th>
                                            <th className='clmEmail'>User Type</th>
                                            <th className='text-center clmStatus'>Status</th>
                                            <th className='text-center clmDate'>Last Login At</th>
                                            <th className='text-center clmDate'>Created At</th>
                                            <th className='text-center clmAction'>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { state.in_prog && <tr><td colSpan={8} className='text-center'><i className='fa fa-refresh fa-spin'></i> Loading..</td></tr>}
                                        { !state.in_prog && state.users.length == 0 && <tr><td colSpan={8} className='text-center'> No Data </td></tr>}
                                        {
                                            !state.in_prog &&  state.users.map((user,i)=>{
                                                var type  =  "Bid Team";
                                                if(user.user_type  == 1){
                                                    type = "Admin";
                                                } else {
                                                    type = "Trainer";
                                                }

                                                
                                                var last_login_at   =   "-";
                                                if(user.last_login_at != null){
                                                    last_login_at   =   moment( user.last_login_at).format(Enviorment.getAppDateFormat('DATETIME'))
                                                }
                                                var createdAt   =   moment( user.createdAt).format(Enviorment.getAppDateFormat('DATETIME'))

                                                var url         =   "/app/admin-user-profile/"+user._id;
                                                var status      =   'Blocked';
                                                if(user.status == false || user.status == null){
                                                    status  =   'Unblocked'
                                                }
                                                return  <tr key={i}>
                                                            <td className='text-center'>{ page_start_index+i + 1}</td>
                                                            <td>{ user.name }</td>
                                                            <td>{ user.email }</td>
                                                            <td >{ type }</td>
                                                            <td className='text-center'>{ status }</td>
                                                            <td className='text-center'>{ last_login_at }</td>
                                                            <td className='text-center'>{ createdAt }</td>
                                                            <td className='text-center'>
                                                                    <ButtonGroup bsSize="xsmall">
                                                                        <Link className='btn btn-default' to={url}>View</Link>
                                                                    </ButtonGroup>
                                                            </td>
                                                        </tr>
                                       total_page     })
                                        }
                                    </tbody>
                                </Table>
                            </div>
                            <div className='text-right'>
                                { total_page > 1 && <CPagination page={state.filter.page} totalpages={total_page} onPageChange={this.changePage}/>}
                            </div>
                        </div>
                    </div>
                </section>
            </Container>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(UserList))
import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {Button,Alert,FormControl} from 'react-bootstrap';
import {get as getUser,update as updateUser} from "../../../../../actions/useraction";

import UserProfileContainer from "../container.jsx";
import {show_popup} from "./../../../../../actions/alertpopupaction";

class BlockUnblock extends React.Component {
    constructor(props){
        super(props);
        this.state  =   {
            'id':'',
            'name':'',
            'name_error':'',
            'email':'',
            'status':false,
            'error':'',
            'success':'',
            'inProg':false
        };
        this.init   =   this.init.bind(this);
        this.update   =   this.update.bind(this);
    }

    componentWillMount(){
            this.init();
    }
    
    init(){
        var me         =    this;
        getUser(this.props.match.params.id).then((user)=>{
            console.log(user);
            me.setState({
                'id':user._id,
                'name':user.name,
                'email':user.email,
                'status':user.status==null?false:user.status
            });
        });
    }

    update(){
        this.setState({'error':'','success':''});

        var me          =       this;
        me.setState({'inProg':true});
        var status  =   "true";
        if(me.state.status){
            status = "false";
        }

        updateUser(me.state.id,{'status':status}).then(()=>{
            me.setState({'status':!me.state.status});
            if(me.state.status){
                me.setState({'success':''});
                me.props.dispatch(show_popup("Information","Profile is  blocked successfully."));
            } else {
                me.setState({'success':''});
                me.props.dispatch(show_popup("Information","Profile is  unblocked successfully."));
            }
            me.setState({'inProg':false});
        }).catch(error=>{
            if(error.error != undefined){
                me.setState({'error':error.error});
            } else {
                console.log(error);
                me.setState({'error':"Something went wrong"});
            }
            me.setState({'inProg':false});
        });
    }

    render() {
        var state   =   this.state;
        if(state.id == null){
            return <UserProfileContainer page={'myprofile'}></UserProfileContainer>;
        }

        var btn_name    =   "Block";
        if(state.status){
            btn_name    =   "Unblock"
        }
        return (
            <UserProfileContainer page={'blockunblock'}>
                <div className="box box-success">
                    <div className="box-header with-border">
                        <h3 className="box-title">Block/Unblock User {state.name}</h3>
                    </div>
                    <div className="box-body">
                        <div className='row'>
                            <div className='col-lg-1'></div>
                            <div className='col-lg-6'>
                                <div className='form'>
                                    {state.error.length != 0 && <Alert bsStyle="danger" >{state.error}</Alert>}
                                    {state.success.length != 0 && <Alert bsStyle="success" >{state.success}</Alert>}
                                    {!state.status && <p>By Clicking on <b>Block</b>, user will no longer able to login to application. Until super admin of the application is manually unblock the user.</p>}
                                    {state.status && <p>By Clicking on <b>UnBlock</b>, user will able to login to application.</p>}
                                    <hr/>
                                    <div className='text-center'>
                                        {!state.inProg && <Button bsStyle='danger' tabIndex={2} onClick={this.update}>{ btn_name }</Button>}
                                        {state.inProg && <Button bsStyle='danger' tabIndex={2} ><i className="fa fa-spinner fa-spin" style={{fontSize:"20px"}}></i></Button>}
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-2'></div>
                        </div>
                    </div>
                </div>
            </UserProfileContainer>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(BlockUnblock))
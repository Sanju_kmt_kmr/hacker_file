import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {Button,Alert,FormControl} from 'react-bootstrap';
import {get as getUser,update as updateUser} from "../../../../../actions/useraction";
import * as Validate from "./../../../../../actions/validation";
import {upload} from "./../../../../../actions/mediaaction";

import UserProfileContainer from "../container.jsx";

import "./style.scss";

class UserProfileGeneral extends React.Component {
    constructor(props){
        super(props);
        this.state  =   {
            'id':'',
            'name':'',
            'name_error':'',
            'email':'',
            'user_type':'',
            'error':'',
            'success':'',
            'inProg':false, 
            'profile_image_path':'/images/default_user.png',
            'imageUploadProgress':false,
        };
        this.init   =   this.init.bind(this);
        this.update   =   this.update.bind(this);

        this.changeProfilePicture   =   this.changeProfilePicture.bind(this);
        this.openFileBrowser        =   this.openFileBrowser.bind(this);
    }

    componentWillMount(){
            this.init();
    }
    
    init(){
        var me         =    this;
        getUser(this.props.match.params.id).then((user)=>{
            console.log(user.profile_image_path);
            me.setState({
                'id':user._id,
                'name':user.name,
                'email':user.email,
                'user_type':parseInt(user.user_type+''),
                'profile_image_path':user.profile_image_path == null?'/images/default_user.png':user.profile_image_path
            });
        });
    }

    componentDidMount(){
        setTimeout(function(){
        },1000);
        
    }

    changeProfilePicture(event){
        if(this.state.imageUploadProgress){
            return ;
        }
        this.setState({'error':'','success':'','imageUploadProgress':false});
        var files   =   event.target.files;
        if(files.length > 0){
            var file    =   files[0];
            if(!Validate.validateImage(file)){
                this.setState({'error':'Please select valid image file.'});
                return;
            }

            var me  =   this;
            this.setState({'imageUploadProgress':true});
            var profile_pic_url     = '/images/default_user.png';
            upload(file).then((response)=>{
                console.log('File Upload ho gyi');
                console.log(response);
                profile_pic_url     =   response.path;
                this.setState({'profile_image_path':profile_pic_url});
                this.update();
                // return updateUser(me.state.id,{'name':me.state.name,'profile_pic':response.id})
            }).then((response)=>{
                // me.setState({'success':'Profile image updated.','profile_image_path':profile_pic_url});
                // me.props.dispatch(CurrentUserThunk());
                me.setState({'imageUploadProgress':false});
            }).catch(error=>{
                me.setState({'error':'Failed to upload the image.'});
                me.setState({'imageUploadProgress':false});
            });
        }
    }

    openFileBrowser(){
        this.imageInput.value = null;
        this.imageInput.click();
    }

    update(){
        this.setState({'error':'','name_error':'','success':''});
        var name       =       this.state.name;
        if(name == undefined  || name == null || name.trim().length == 0){
            this.setState({'name_error':"Please provide name."})
            document.getElementById('profile_name').focus();
            return;
        }   

        var user_type   =   this.state.user_type;

        var me          =       this;
        me.setState({'inProg':true});
        updateUser(
            me.state.id,
            {
                'name':name,
                'user_type':user_type,
                'profile_image_path': me.state.profile_image_path,
        }).then(()=>{
            me.setState({'success':''});
            me.props.dispatch(show_popup("Information","Profile updated successfully.",()=>{
                me.props.history.push("/app/admin-user/list");
            }));
            me.setState({'inProg':false});
            document.getElementById('profile_name').focus();
            // this.props.dispatch(CurrentUserThunk());
        }).catch(error=>{
            if(error.error != undefined){
                me.setState({'error':error.error});
            } else {
                console.log(error);
                me.setState({'error':"Something went wrong"});
            }
            me.setState({'inProg':false});
        });
    }

    render() {
        var state   =   this.state;
        if(state.id == null){
            return <UserProfileContainer page={'myprofile'}></UserProfileContainer>;
        }

        var edit_caption_class  =   "edit_caption";
        if(state.imageUploadProgress){
            edit_caption_class  =   "edit_caption visible";
        }

        var name_input_class   =   "form-group  ";
        if(state.name_error.length != 0){
            name_input_class   = "form-group  error_input";
        }

        var user_type_input_class   =   "form-group  ";
        

        var     user_type_options   =   [
            {
                'name':'Admin',
                'value':2
            },
            {
                'name':'Super Admin',
                'value':1
            }
        ];


        return (
            <UserProfileContainer page={'myprofile'}>
                <div className="box box-success">
                    <div className="box-header with-border">
                        <h3 className="box-title">General Profile</h3>
                    </div>
                    <div className="box-body">
                        <div className='row'>
                            <div className='col-lg-1'></div>
                            <div className='col-lg-3 text-center'>
                                <div className='profile_image_path' onClick={this.openFileBrowser}>
                                    <img src={state.profile_image_path} className='profilePic'/>
                                    <div className={ edit_caption_class }>
                                        {!state.imageUploadProgress && "Edit Profile Picture"}
                                        {state.imageUploadProgress && <i className="fa fa-spinner fa-spin" aria-hidden="true"></i>}
                                    </div>
                                </div>
                                <input type='file' 
                                ref={fileInput => this.imageInput = fileInput} 
                                style={{'visibility':'hidden'}}  onChange={this.changeProfilePicture}/>

                            </div> 
                            <div className='col-lg-6'>
                                <div className='form update-profile'>
                                    {state.error.length != 0 && <Alert bsStyle="danger" >{state.error}</Alert>}
                                    {state.success.length != 0 && <Alert bsStyle="success" >{state.success}</Alert>}
                                    <div className={name_input_class}>
                                        <label>Name*</label>
                                        <input className="form-control" id='profile_name' type='text' tabIndex={1} placeholder="Name" value={state.name} onChange={(event)=>{this.setState({'name':event.target.value})}}/>
                                        {state.name_error.length != 0 && <div className='error'>{state.name_error}</div>}
                                    </div>
                                    <div className='form-group'>
                                        <label>Email*</label>
                                        <input className="form-control" type='text' placeholder="Email" value={state.email} onChange={(event)=>{}} readOnly/>
                                    </div>
                                    <hr/>
                                    <div className='text-right'>
                                        {!state.inProg && <Button bsStyle='default' className='bg-purple' tabIndex={2} onClick={this.update}>Update</Button>}
                                        {state.inProg && <Button bsStyle='default' className='bg-purple' tabIndex={2} ><i className="fa fa-spinner fa-spin" style={{fontSize:"20px"}}></i></Button>}
                                        &nbsp;&nbsp;
                                        <Button tabIndex={3} onClick={this.init}>Cancel</Button>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-2'></div>
                        </div>
                    </div>
                </div>
            </UserProfileContainer>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(UserProfileGeneral))
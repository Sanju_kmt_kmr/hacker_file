import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {get as getUser} from "../../../../actions/useraction";
import Container from "../../Container/index.jsx";
import ProfileNav from "./sidenav.jsx";

import "./style.scss";

class UserProfileContainer extends React.Component {

    constructor(props){
        super(props);
        this.state =    {
            'id':null
        }
    }
    componentWillMount(){
        this.init();
    }

    init(){
        var me         =    this;
        getUser(this.props.match.params.id).then((user)=>{
            me.setState({
                'id':user._id,
            });
        });
    }

    render() {
        if(this.state.id == null){
            return <Container ></Container>;
        }

        var page   =   this.props.page;
        if(page == undefined){
            page    =   "profile";
        }

        return (
            <Container nav_name={'users'}>
                <section className="content">
                    <div className="box box-success">
                        <div className="box-header with-border">
                            <h3 className="box-title">User Profile </h3>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-lg-3'>
                            <ProfileNav page={page} id={this.state.id}/>
                        </div>
                        <div className='col-lg-9'>
                            { this.props.children}
                        </div>
                    </div>
                </section>
            </Container>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(UserProfileContainer))
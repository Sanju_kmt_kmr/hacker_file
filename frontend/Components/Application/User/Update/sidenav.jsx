import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import { Nav,NavItem} from 'react-bootstrap';


class ProfileNav extends React.Component {
    constructor(props){
        super(props);
        this.handleSelect       =   this.handleSelect.bind(this);
    }
    componentWillMount(){
    }
    
    handleSelect(){

    }
    render() {
        var page   =   this.props.page;
        if(page == undefined){
            page    =   "myprofile";
        }

        var activeKey   =   1;
        if(page == 'myprofile'){
            activeKey   =   1;
        } else if(page == 'changepassword'){
            activeKey   =   2;
        } else {
            activeKey   =   3;
        }
        var general_profile_url     =   "#/app/admin-user-profile/"+this.props.id;
        var general_password_url    =   "#/app/admin-user-profile/"+this.props.id+"/password";
        var block_unblock_url       =   "#/app/admin-user-profile/"+this.props.id+"/block-unblock";

        return (
            <div className="box box-success">
                <div className="box-body">
                    <Nav bsStyle="pills" stacked activeKey={activeKey} onSelect={this.handleSelect}>
                        <NavItem eventKey={1} href={general_profile_url }>
                            General Profile
                        </NavItem>
                        <NavItem eventKey={2} href={ general_password_url }>
                            Change Password
                        </NavItem>
                        <NavItem eventKey={3} href={ block_unblock_url }>
                            Block/Unblock User
                        </NavItem>
                    </Nav>
                </div>
            </div>
        );
    }

}

export default withRouter(connect(function(store){
    return {
        currentUser:store.currentUser
    };
})(ProfileNav))
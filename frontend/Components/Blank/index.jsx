import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import Logo from "./../Common/Logo/index.jsx";
import {authenticate,me as CurrentUser} from "../../actions/useraction";
import {Checkbox} from 'react-icheck';
import * as Validate from "../../actions/validation";
import { Alert } from 'react-bootstrap';

import "./style.scss";

class Blank extends React.Component {

    constructor(props){
        super(props);
    }

    componentDidMount(){
        $('body').attr('class','hold-transition login-page');
        setTimeout(function(){
            $(".login-logo-blank").addClass('animate');
        },1000);
        var me         =    this;
        CurrentUser().then(()=>{
            me.props.history.push("/app")
        }).catch(error=>{
            setTimeout(function(){
                me.props.history.push("/login");
            },2000)
        })
    }

    render() {
        return (
            <div className="login-box">
                <div className="login-logo-blank">
                    <Logo />
                    {/* <br />
                    <br />
                    <br />
                    <p style={{'color':'#00a757','fontSize':'20px'}}>Bid Management made easy for your Organization.</p> */}
                </div>
            </div>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(Blank))
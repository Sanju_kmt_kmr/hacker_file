import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';

class Footer extends React.Component {
    componentWillMount(){
    }
    render() {
        return (
            <footer className="main-footer">
                <div className="pull-right hidden-xs">
                <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; 2018-2019 <a href="http//www.letsgo.hirarky.com">Socially Fit</a>.</strong> All rights reserved.
            </footer>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(Footer))
import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';

class Logo extends React.Component {
    render() {
        return (
            <h1 className='app_logo_new'>Hacker</h1>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(Logo))
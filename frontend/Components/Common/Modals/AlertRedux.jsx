import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {Modal,Button} from 'react-bootstrap';
import "./style.scss";
import {hide_popup} from "../../../actions/alertpopupaction";

class Alert extends React.Component {
    constructor(props){
        super(props);
        this.handleClose    =   this.handleClose.bind(this);
    }
    
    handleClose(){
        this.props.dispatch(hide_popup());
        if(this.props.popup.close != undefined && this.props.popup.close != null){ 
            this.props.popup.close();
        }
    }

    render(){
        var title   = "Alert";
        if(this.props.popup.title != undefined && this.props.popup.title != null){
            title   = this.props.popup.title;
        }
        return <Modal show={this.props.popup.show} onHide={this.handleClose} backdrop={'static'} keyboard={false} animation={false}>
                    <Modal.Header>
                        <Modal.Title>{title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>{this.props.popup.message}</p> 
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Ok</Button>
                    </Modal.Footer>
                </Modal>
    }
}

export default withRouter(connect(function(store){
    return {
        popup:store.alertpopup
    };
})(Alert))
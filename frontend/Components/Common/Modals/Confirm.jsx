import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {Modal,Button} from 'react-bootstrap';
import "./style.scss";


class Confirm extends React.Component {
    constructor(props){
        super(props);
        this.handleClose    =   this.handleClose.bind(this);
        this.handleYes      =   this.handleYes.bind(this);
    }
    
    handleClose(){
        if(this.props.close != undefined){
            this.props.close();
        }
    }

    handleYes(){
        if(this.props.yes != undefined){
            this.props.yes();
        }
    }

    render(){

        var title   = "Confirm";
        if(this.props.title != undefined && this.props.title != null){
            title   = this.props.title;
        }
        return <Modal show={this.props.show} onHide={this.handleClose} backdrop={'static'} keyboard={false} animation={false}>
                    <Modal.Header>
                        <Modal.Title>{title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>{this.props.message}</p> 
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="success" onClick={this.handleYes}>Confirm</Button>
                        <Button onClick={this.handleClose}>Cancel</Button>
                    </Modal.Footer>
                </Modal>
    }
}

export default withRouter(connect(function(store){
    return {
    };
})(Confirm))
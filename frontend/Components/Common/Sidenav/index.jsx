import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {Link} from 'react-router-dom';

import UserPannel from "./UserPannel/index.jsx";

class Sidenav extends React.Component {
    constructor(props){
        super(props);
        this.handleMenuToggle     =     this.handleMenuToggle.bind(this);
    }
    componentWillMount(){
    }

    handleMenuToggle(event){
        event.preventDefault();
        console.log(555);
        var element   =  event.currentTarget
        var className = element.getAttribute('class');
        if(className.indexOf('menu-open') == -1){
            element.classList.add("menu-open");
            element.querySelector(".treeview-menu").style.display = "block";
        } else {
            element.classList.remove("menu-open");
            element.querySelector(".treeview-menu").style.display = "none";
        }

    }
    render() {

        var menus        =   [];
        
        var nav_name = this.props.nav_name;

        menus.push({
            'link':"/app/trainer/list",
            'label':'Trainer Management',
            'icon':'fa fa-trophy',
            'nav':["trainer"],
        },{
            'link':"/app/buddy/list",
            'label':'Buddy Management',
            'icon':'fa fa-trophy',
            'nav':["buddy"],
        },{
            'link':"#",
            'label':'Master',
            'icon':'fa fa-th',
            'nav':["locations","trainertype"],
            'children':[
                {
                    'link':"/app/location/list",
                    'label':'Master Locations',
                    'icon':'fa fa-circle-o text-red',
                    'nav':["locations"]
                },{
                    'link':"/app/trainer-type/list",
                    'label':'Master Trainer Types',
                    'icon':'fa fa-circle-o text-yellow',
                    'nav':["trainertype"]
                }, {
                    'link':"/app/fitness-goals/list",
                    'label':'Master Fitness Goals',
                    'icon':'fa fa-circle-o text-green',
                    'nav':["fitness-goals"]
                },{
                    'link':"/app/workout-types/list",
                    'label':'Master Workout Types',
                    'icon':'fa fa-circle-o text-green',
                    'nav':["workout-types"]
                },
            ]
        },{
            'link':"/app/admin-user/list",
            'label':'Admin Users',
            'icon':'fa fa-user',
            'nav':["users"]
        });

        
        return (
            <aside className="main-sidebar">
                <section className="sidebar">
                    <UserPannel />
                    <ul className="sidebar-menu" data-widget="tree">
                        <li className="header">MAIN NAVIGATION</li>
                        {
                            menus.map((menu,k)=>{
                                
                                if(menu.children == undefined){
                                    var className   =   "";
                                    if(menu.nav.indexOf(nav_name) != -1){
                                        className   =   "active";
                                    }

                                    return  <li key={k} className={className}>
                                            <Link to={menu.link}>
                                                <i className={menu.icon}></i> <span>{menu.label}</span>
                                            </Link>
                                        </li>;
                                } else {
                                    var className   =   "treeview";
                                    if(menu.nav.indexOf(nav_name) != -1){
                                        className   =   "treeview active menu-open";
                                    }
                                    return <li key={k} className={ className } onClick={this.handleMenuToggle}>
                                        <a href="javascript:void(0)">
                                            <i className={menu.icon}></i>
                                            <span>{menu.label}</span>
                                            <span className="pull-right-container">
                                            <i className="fa fa-angle-left pull-right"></i>
                                            </span>
                                        </a>
                                        <ul className="treeview-menu" >
                                            {
                                                menu.children.map((cmenu,l)=>{

                                                    var _className   =   "";
                                                    if(cmenu.nav.indexOf(nav_name) != -1){
                                                        _className   =   "active";
                                                    }

                                                    return <li key={l} className={_className}>
                                                        <Link to={cmenu.link}>
                                                            <i className={cmenu.icon}></i> <span>{cmenu.label}</span>
                                                        </Link>
                                                    </li>
                                                })
                                            }
                                        </ul>
                                    </li>;
                                }
                                
                            })
                        }
                    </ul>
                </section>
            </aside>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(Sidenav))
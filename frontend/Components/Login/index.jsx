import React from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import Logo from "./../Common/Logo/index.jsx";
import {authenticate,me as CurrentUser} from "../../actions/useraction";
import {Checkbox} from 'react-icheck';
import * as Validate from "../../actions/validation";
import { Alert } from 'react-bootstrap';

import "./style.scss";

class Login extends React.Component {

    constructor(props){
        super(props);
        this.state  =   {
            'email':'',
            'password':'',
            'remember_me':'false',
            'email_error':'',
            'password_error':'',
            'error':'',
            'inProg':false
        };
        this.login          =   this.login.bind(this);
        this.handleEnter    =   this.handleEnter.bind(this);
        this.handleRememberMe   =   this.handleRememberMe.bind(this);
    }

    componentDidMount(){
        $('body').attr('class','hold-transition login-page');
        document.getElementById('inputEmail').focus();

        var me         =    this;
        CurrentUser().then(()=>{
            me.props.history.push("/app")
        }).catch(error=>{
            console.log(error);
        })
    }
    
    handleEnter(event){
        if (event.key === 'Enter') {
            this.login();
        }
    }

    handleRememberMe(event){
        if(event.target.checked){
            this.setState({'remember_me':'true'});
        } else {
            this.setState({'remember_me':'false'});
        }
    }

    login(){
        this.setState({'email_error':"","password_error":"","error":""});

        var email       =       this.state.email;
        if(email == undefined  || email == null || email.trim().length == 0){
            this.setState({'email_error':"Please provide email to login."})
            document.getElementById('inputEmail').focus();
            return;
        }   

        console.log(Validate.email(email));
        if(!Validate.email(email)){
            this.setState({'email_error':"Please provide valid email id to login."});
            document.getElementById('inputEmail').focus();
            return;
        }

        var password       =       this.state.password;
        if(password == undefined  || password == null || password.trim().length == 0){
            this.setState({'password_error':"Please provide password to login."})
            document.getElementById('inputPassword').focus();
            return;
        }

        var user_type = 1;

        var me      =   this;
        me.setState({'inProg':true});
        authenticate(email,password,this.state.remember_me,user_type).then(()=>{
            me.props.history.push("/app");
            me.setState({'inProg':false});
        }).catch(error=>{
            if(error.error != undefined){
                me.setState({'error':error.error});
            } else {
                console.log(error);
                me.setState({'error':"Something went wrong"});
            }
            me.setState({'inProg':false});
        });
    }

    render() {
        var state   =    this.state;
        var remember_me     =   false;
        if(state.remember_me == "true"){
            remember_me     =   true;
        }
        
        var email_input_class   =   "form-group  has-feedback";
        if(state.email_error.length != 0){
            email_input_class   = "form-group  has-feedback error_input";
        }

        var password_input_class   =   "form-group  has-feedback";
        if(state.password_error.length != 0){
            password_input_class   = "form-group  has-feedback error_input";
        }

        console.log(state.error);
        
        return (
            <div className="login-box">
                <div className="login-logo">
                    <Logo />
                </div>
                <div className="login-box-body">
                    <p className="login-box-msg">Sign in to start your session</p>
                    <div>
                        {state.error.length != 0 && <Alert bsStyle="danger" >{state.error}</Alert>}
                        <div className={email_input_class}>
                            <input type="email" className="form-control" placeholder="Email" id="inputEmail" tabIndex={1} value={state.email} 
                            onChange={(event)=>{this.setState({'email':event.target.value})}}
                            onKeyPress={this.handleEnter}
                            />
                            <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div className='error'>{ state.email_error }</div>
                        <div className={password_input_class}>
                            <input type="password" className="form-control" placeholder="Password" id="inputPassword" tabIndex={2} value={state.password} 
                            onChange={(event)=>{this.setState({'password':event.target.value})}}
                            onKeyPress={this.handleEnter}/>
                            <span className="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div className='error' style={{'marginBottom':'0'}}>{ state.password_error }</div>
                        <div className='text-right'>
                            <Link to="/forgot-password" className='theme-color'>Forgotten Password?</Link>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-xs-8">
                                <div className="checkbox icheck">
                                    <Checkbox
                                        id="remember_me"
                                        checkboxClass="icheckbox_square-blue"
                                        increaseArea="20%"
                                        value={remember_me}
                                        onChange={this.handleRememberMe}
                                    /> &nbsp;
                                    <label htmlFor="remember_me">Remeber Me?</label>
                                </div>
                            </div>
                            <div className="col-xs-4">
                                {!state.inProg && <button type="submit" tabIndex={3} className="btn bg-purple btn-block btn-flat" onClick={this.login}>Sign In</button>}
                                {state.inProg && <button type="submit" tabIndex={3} className="btn bg-purple btn-block btn-flat"><i className="fa fa-spinner fa-spin" style={{fontSize:"24px"}}></i></button>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default withRouter(connect(function(store){
    return {
    };
})(Login))
export function show_popup(title,message,close){
    return {
        'type':'SHOW_ALERT',
        'payload':{
            'title':title,
            'message':message,
            'close':close
        }
    };
}

export function hide_popup(){
    return {
        'type':'HIDE_ALERT',
        'payload':{
        }
    };
}

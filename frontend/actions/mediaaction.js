/*
 |  File useraction.js contains the actions related to user
 |  like login logout fetch user details
 |
 |
 */

import * as Enviorment from "./enviorment";
import axios from "axios";
import * as q from 'q';

//Method is used to update user.
export function upload(file){
    var defer                   =   q.defer();
    
    var host                =   Enviorment.host();
    var url                 =   host+"/api/media/add?nonnce="+(new Date()).getTime();

    var DEBUG               =   Enviorment.debug();

    const formData = new FormData();
    formData.append('file',file)
    axios.post(url,formData,{
        headers:{
            'authorization':'Bearer '+window.localStorage.getItem("auth_token",null)
        }
    })
    .then(function(response){
        DEBUG && console.log(response);        
        if(response.data != undefined  && response.data.statusCode == 200 && response.data.data != undefined){
            var response_data   =   response.data.data;
            defer.resolve(response_data);
        } else {
            DEBUG &&  console.log("Error",response);
            defer.reject({'error':"Something went wrong!"});
        }
    }).catch(function(error){
        if(error.response != undefined && error.response.data != undefined && error.response.data.message != undefined){
            DEBUG &&  console.log("Error",error.response.data);
            defer.reject({'error':error.response.data.message})
        } else {
            DEBUG &&  console.log("Error",error);
            defer.reject({'error':"Something went wrong!"});
        }
    });

    return defer.promise;
}
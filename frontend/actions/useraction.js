/*
 |  File useraction.js contains the actions related to user
 |  like login logout fetch user details
 |
 |
 */

import * as Enviorment from "./enviorment";
import axios from "axios";
import * as q from 'q';

//Method is used to authenticate the user.
export function authenticate(email,password,remember_me,user_type){
    var defer                   =   q.defer();

    var host                =   Enviorment.host();
    var url                 =   host+"/api/user/authenticate";
    var DEBUG               =   Enviorment.debug();

    DEBUG && console.log("Login request for email : "+email+" remember_me "+remember_me);
    axios.post(url,{
        email:email,
        password:password,
        remember_me:remember_me,
        user_type:user_type
    })
    .then(function(response){
        DEBUG && console.log(response);        
        if(response.data != undefined && response.data.statusCode == 200 && response.data.data != undefined){
            var response_data   =   response.data.data;
            var token           =   response_data.token;
            if(token == undefined){
                DEBUG &&  console.log("Error : token is not defined in response which is recived.");
                defer.reject({'error':"Something went wrong!"});
            } else {
                window.localStorage.setItem("auth_token",token);
                defer.resolve({});
            }
        } else {
            DEBUG &&  console.log("Error",response);
            defer.reject({'error':"Something went wrong!"});
        }
    }).catch(function(error){
        if(error.response != undefined && error.response.data != undefined && error.response.data.message != undefined){
            DEBUG &&  console.log("Error",error.response.data);
            defer.reject({'error':error.response.data.message})
        } else {
            DEBUG &&  console.log("Error",error);
            defer.reject({'error':"Something went wrong!"});
        }
    });

    return defer.promise;
}

//Method is used to currently authenticate user.
export function me(cached){
    if(cached == undefined){
        cached  = false;
    }
    
    var defer                   =   q.defer();

    if(cached){
        var user    =   window.sessionStorage.getItem("auth_user",null);
        if(user != null){
            defer.resolve(JSON.parse(user));
            return defer.promise;
        }
    }
    
    var host                =   Enviorment.host();
    var url                 =   host+"/api/user/me";
    var DEBUG               =   Enviorment.debug();

    axios.get(url,{
        headers:{
            'authorization':'Bearer '+window.localStorage.getItem("auth_token",null)
        }
    })
    .then(function(response){
        DEBUG && console.log(response);        
        if(response.data != undefined ){
            var response_data   =   response.data;
            window.sessionStorage.setItem("auth_user",JSON.stringify(response_data));
            defer.resolve(response_data);
        } else {
            DEBUG &&  console.log("Error",response);
            defer.reject({'error':"Something went wrong!"});
        }
    }).catch(function(error){
        if(error.response != undefined && error.response.data != undefined && error.response.data.message != undefined){
            DEBUG &&  console.log("Error",error.response.data);
            defer.reject({'error':error.response.data.message})
        } else {
            DEBUG &&  console.log("Error",error);
            defer.reject({'error':"Something went wrong!"});
        }
    });

    return defer.promise;
}

//Method is used to currently authenticate user.
export function logout(){
    var defer                   =   q.defer();

    window.sessionStorage.removeItem("auth_user",null);
    window.localStorage.removeItem("auth_token",null);
    defer.resolve({});

    return defer.promise;
}

//Method is used to update user.
export function update(id,data){
    console.log('Calling Action');
    var defer                   =   q.defer();
    var host                =   Enviorment.host();
    var url                 =   host+"/api/user/"+id;
    var DEBUG               =   Enviorment.debug();
    axios.put(url,data,{
        headers:{
            'authorization':'Bearer '+window.localStorage.getItem("auth_token",null)
        }
    })
    .then(function(response){
        DEBUG && console.log(response);        
        if(response.data != undefined  && response.data.statusCode == 200 && response.data.data != undefined){
            var response_data   =   response.data.data;
            defer.resolve(response_data);
        } else {
            DEBUG &&  console.log("Error",response);
            defer.reject({'error':"Something went wrong!"});
        }
    }).catch(function(error){
        if(error.response != undefined && error.response.data != undefined && error.response.data.message != undefined){
            DEBUG &&  console.log("Error",error.response.data);
            defer.reject({'error':error.response.data.message})
        } else {
            DEBUG &&  console.log("Error",error);
            defer.reject({'error':"Something went wrong!"});
        }
    });
    return defer.promise;
}


//Method is used to get user.
export function get(_id){
    console.log(_id);
    var defer                   =   q.defer();
    
    var host                =   Enviorment.host();
    var url                 =   host+"/api/user/"+_id;
    var DEBUG               =   Enviorment.debug();

    axios.get(url,{
        headers:{
            'authorization':'Bearer '+window.localStorage.getItem("auth_token",null)
        }
    })
    .then(function(response){
        // DEBUG && console.log(response);        
        if(response.data != undefined  && response.data.statusCode == 200 && response.data.data != undefined){
            var response_data   =   response.data.data;
            defer.resolve(response_data);
        } else {
            DEBUG &&  console.log("Error",response);
            defer.reject({'error':"Something went wrong!"});
        }
    }).catch(function(error){
        if(error.response != undefined && error.response.data != undefined && error.response.data.message != undefined){
            DEBUG &&  console.log("Error",error.response.data);
            defer.reject({'error':error.response.data.message})
        } else {
            DEBUG &&  console.log("Error",error);
            defer.reject({'error':"Something went wrong!"});
        }
    });

    return defer.promise;
}


//Method is used to add user.
export function add(data){
    var defer                   =   q.defer();
    
    var host                =   Enviorment.host();
    var url                 =   host+"/api/user_expense/add";
    var DEBUG               =   Enviorment.debug();

    axios.post(url,data,{
        headers:{
            'authorization':'Bearer '+window.localStorage.getItem("auth_token",null)
        }
    })
    .then(function(response){
        DEBUG && console.log(response);        
        if(response.data != undefined  && response.data.statusCode == 200 && response.data.data != undefined){
            var response_data   =   response.data.data;
            defer.resolve(response_data);
        } else {
            DEBUG &&  console.log("Error",response);
            defer.reject({'error':"Something went wrong!"});
        }
    }).catch(function(error){
        if(error.response != undefined && error.response.data != undefined && error.response.data.message != undefined){
            DEBUG &&  console.log("Error",error.response.data);
            defer.reject({'error':error.response.data.message})
        } else {
            DEBUG &&  console.log("Error",error);
            defer.reject({'error':"Something went wrong!"});
        }
    });

    return defer.promise;
}



//Method is used to currently authenticate user.
export function me_thunk(){
    var host                =   Enviorment.host();
    var url                 =   host+"/api/user/me";
    var DEBUG               =   Enviorment.debug();
    return function(dispatch){
        axios.get(url,{
            headers:{
                'authorization':'Bearer '+window.localStorage.getItem("auth_token",null)
            }
        })
        .then(function(response){
            // DEBUG && console.log(response);        
            if(response.data != undefined ){
                var response_data   =   response.data;
                window.sessionStorage.setItem("auth_user",JSON.stringify(response_data));
                dispatch({'type':'USER_UPDATED',payload:response_data});
            } else {
                DEBUG &&  console.log("Error",response);
            }
        }).catch(function(error){
            if(error.response != undefined && error.response.data != undefined && error.response.data.message != undefined){
                DEBUG &&  console.log("Error",error.response.data);
            } else {
                DEBUG &&  console.log("Error",error);
            }
        });
    }

}



//Method is used to fetch users
export function fetchUsers(data){
    var defer                   =   q.defer();
    var host                =   Enviorment.host();
    var url                 =   host+"/api/user_expense";
    var DEBUG               =   Enviorment.debug();
    axios.post(url,data,{
        headers:{
            'authorization':'Bearer '+window.localStorage.getItem("auth_token",null)
        }
    })
    .then(function(response){
        // DEBUG && console.log(response);        
        if(response.data != undefined  && response.data.statusCode == 200 ){
            var response_data   =   response.data;
            defer.resolve(response_data);
        } else {
            DEBUG &&  console.log("Error",response);
            defer.reject({'error':"Something went wrong!"});
        }
    }).catch(function(error){
        if(error.response != undefined && error.response.data != undefined && error.response.data.message != undefined){
            DEBUG &&  console.log("Error",error.response.data);
            defer.reject({'error':error.response.data.message})
        } else {
            DEBUG &&  console.log("Error",error);
            defer.reject({'error':"Something went wrong!"});
        }
    });

    return defer.promise;
}

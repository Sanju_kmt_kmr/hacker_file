/*
 |  File validation.js is used to validate the application.
 |
 */


//Method is used to validate the email
export function email(value){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(value);
}


//Method is used to validate the image file
export function validateImage(file){
    var fileType = file["type"];
    console.log(fileType);
    var ValidImageTypes = [ "image/jpeg", "image/png","image/jpg"];
    if (ValidImageTypes.indexOf(fileType) != -1) {
        // invalid file type code goes here.
        return true;
    }
    return false
}
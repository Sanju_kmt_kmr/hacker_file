import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App.jsx';
import {Provider} from 'react-redux';
import store   from "./store";
import { Router } from 'react-router-dom'
import { createHashHistory } from 'history';

const history = createHashHistory({ queryKey: false });

// Router, hashHistory
ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>, document.getElementById('app'));
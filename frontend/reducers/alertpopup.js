export default function reducer(state={show:false,title:"",message:"",close:null},action){
    switch(action.type){
        case "SHOW_ALERT":{
            var payload     =   action.payload;
            return {...state,show:true,title:payload.title,message:payload.message,close:payload.close}
            break;
        }
        case "HIDE_ALERT":{
            var payload     =   action.payload;
            return {...state,show:false,title:"",message:"",close:""}
            break;
        }
        default:{
            return state;
        }
    }

}

import {combineReducers} from "redux";

import currentUser  from "./currentUser";
import alertpopup  from "./alertpopup";

export default combineReducers({
    currentUser,
    alertpopup
});

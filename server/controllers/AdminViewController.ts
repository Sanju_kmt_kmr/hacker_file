/*
 |-------------------------------------------------------------------
 |  FontController.ts - controller to handle the api for Fonts
 |-------------------------------------------------------------------
 */
var view  =  require('./../../render');

export class AdminViewController {

    //Fetch the font in bulk
    public static index(request,response){
        response.send(view.admin());
    }

}

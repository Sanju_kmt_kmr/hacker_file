/*
 |-------------------------------------------------------------------
 |  UserController.ts - controller to handle the api for users
 |-------------------------------------------------------------------
 */

import {Validation} from '../core/Util/Validation';
import {Message} from '../core/Util/Message';
import { Storage } from '../core/Storage';
import * as fs from 'fs';
import * as path from 'path';
import * as mime from 'mime';

export class MediaController {

    public static index(request,response){
        var params  =   request.body;

    }

    //Controller method to authenticate the user.
    public static show(request,response){
        var input_param  =   request.query;

        var query_path    =   input_param['path'];
        if(query_path == undefined){
            response.status(400);
            response.send("Invalid url.");
            return;
        }

        var baseDirectory   =   Storage.getBaseDirectory();
        var file_path   =   path.join(baseDirectory,query_path);
        if(!fs.existsSync(file_path)){
            response.status(404);
            response.send("Requested resource does not exists.");
            return;
        }

        var stat = fs.statSync(file_path);
        
        response.writeHead(200, {
            'Content-Type': mime.getType(file_path),
            'Content-Length': stat.size
        });

        var readStream = fs.createReadStream(file_path);
        // We replaced all the event handlers with a simple call to readStream.pipe()
        readStream.pipe(response);
    }

     //Controller method to authenticate the user.
    public static showById(request,response){
        var input_param  =   request.params;

        
        if(input_param['id'] == undefined || input_param['id'] == null ||
        input_param['id'].trim().length == 0 || isNaN(input_param['id'])){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Id provided is not valid.")).toJson());
            response.end();
            return;
            
        }
        var storageManager  =   Storage.getInstance();
        var id      =   parseInt(input_param['id']);
       
        storageManager.getById(id).then((result:Message)=>{
            response.status(result.getResponseCode());
            response.json(result.toJson());
        }).catch(error=>{
            console.log(error);
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to find",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });


    }


    //Method to add user
    public static add(request,response){
        var input_data  =   request.query;
        
        if (!request.files){
            var message     =   new Message(Message.INVALID_PARAM,"No File was uploaded");
            response.status(message.getResponseCode());
            response.json(message.toJson());
            return;
        }

        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
        let file = request.files.file;
        if(file == undefined){
            var message     =   new Message(Message.INVALID_PARAM,"No File was uploaded");
            response.status(message.getResponseCode());
            response.json(message.toJson());
            return;
        }


        var storageManager  =   Storage.getInstance();
        storageManager.addToStorage(file).then((result:Message)=>{
            response.status(result.getResponseCode());
            response.json(result.toJson());
        }).catch(error=>{
            console.log(error);
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to add file",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });
    }

   
}


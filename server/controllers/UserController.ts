/*
 |-------------------------------------------------------------------
 |  UserController.ts - controller to handle the api for users
 |-------------------------------------------------------------------
 */

import {UserManager} from '../core/UserManager';
import {Validation} from '../core/Util/Validation';
import {Message} from '../core/Util/Message';


export class UserController {

    public static index(request,response){
        var params  =   request.body;

        var userManager         =   UserManager.getInstance();
        userManager.fetchUsers(params).then((result:Message)=>{
            response.status(result.getResponseCode());
            response.json(result.toJson());
        }).catch(error=>{
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to fetch users",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });
    }

    //Controller method to authenticate the user.
    public static authenticate(request,response){
        var input_data  =   request.body;

        var authentication_type     =   "admin";
        if(request.url.indexOf("trainer") != -1){
            authentication_type     =   "trainer";
        }

        if(input_data['email'] == undefined || input_data['email'] == null ||
            input_data['email'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Email provided is empty.")).toJson());
            response.end();
            return;
        }

        if(input_data['password'] == undefined || input_data['password'] == null ||
            input_data['password'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Password provided is empty.")).toJson());
            response.end();
            return;
        }

        if(input_data['user_type'] == undefined || input_data['user_type'] == null){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"User type provided is empty.")).toJson());
            response.end();
            return;
        }

        var fromApp = 'Admin';
        if(input_data['fromApp'] != undefined || input_data['fromApp'] != null){
            fromApp = input_data['fromApp'];
        }

        var userManager         =   UserManager.getInstance();
        var email               =   input_data['email'].trim();
        var password            =   input_data['password'].trim();
        var user_type           =   input_data['user_type'];
        var remember_me         =   false;
        if(input_data['remember_me'] != undefined && input_data['remember_me'] == "true" ){
            remember_me         =   true;
        }

        if(fromApp == 'Admin'){
            userManager.authenticate(email,password,remember_me,user_type).then((result:Message)=>{
                response.status(result.getResponseCode());
                response.json(result.toJson());
            }).catch(error=>{
                if(error instanceof Message){
                    response.status(error.getResponseCode());
                    response.json(error.toJson());
                } else {
                    var message     = new Message(Message.INTERNAL_ERROR,"Failed to authenticate",error);
                    response.status(message.getResponseCode());
                    response.json(message.toJson());
                }
            });
        }else{
            console.log('Buddy Authentic Manager');
            userManager.authenticateBuddy(email,password,remember_me,user_type).then((result:Message)=>{
                response.status(result.getResponseCode());
                response.json(result.toJson());
            }).catch(error=>{
                if(error instanceof Message){
                    response.status(error.getResponseCode());
                    response.json(error.toJson());
                } else {
                    var message     = new Message(Message.INTERNAL_ERROR,"Failed to authenticate",error);
                    response.status(message.getResponseCode());
                    response.json(message.toJson());
                }
            }); 
        }
       


    }

    //Controller method to authenticate the user.
    public static me(request,response){
        response.json(request.auth_user);
    }


    //Controller method to authenticate the user.
    public static show(request,response){
        var input_param  =   request.params;

        if(input_param['id'] == undefined || input_param['id'] == null ||
        input_param['id'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"User id provided is not valid.")).toJson());
            response.end();
            return;
        }
        var userManager         =   UserManager.getInstance();
        var _id      =   (input_param['id']);

        userManager.getUserById(_id).then((result:Message)=>{
            response.status(result.getResponseCode());
            response.json(result.toJson());
        }).catch(error=>{
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to find user",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });


    }


    //Method to add user
    public static add(request,response){
        var input_data  =   request.body;

        if(input_data['name'] == undefined || input_data['name'] == null ||
            input_data['name'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Name provided is empty.")).toJson());
            response.end();
            return;
        }

        if(input_data['email'] == undefined || input_data['email'] == null ||
            input_data['email'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Email provided is empty.")).toJson());
            response.end();
            return;
        }

        if(input_data['password'] == undefined || input_data['password'] == null ||
            input_data['password'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Password provided is empty.")).toJson());
            response.end();
            return;
        }

        if(input_data['user_type'] == undefined || input_data['user_type'] == null ||
            isNaN(input_data['user_type'])){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"User type provided is not valid.")).toJson());
            response.end();
            return;
        }

        
        var userManager         =   UserManager.getInstance();
        var name                =   input_data['name'].trim();
        var email               =   input_data['email'].trim();
        var password            =   input_data['password'];
        var user_type           =   parseInt(input_data['user_type']);

        var data = {};
        data['name'] = name;
        data['email'] = email;
        data['password'] = password;
        data['user_type'] = user_type;
        userManager.addUser(data).then((result:Message)=>{
            response.status(result.getResponseCode());
            response.json(result.toJson());
        }).catch(error=>{
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to add user",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });
    }
    

}


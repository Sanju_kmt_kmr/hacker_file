/*
 |-------------------------------------------------------------------
 |  UserController.ts - controller to handle the api for users
 |-------------------------------------------------------------------
 */

import {UserExpenseManager} from '../core/UserExpenseManager';
import {Validation} from '../core/Util/Validation';
import {Message} from '../core/Util/Message';


export class UserExpenseController {

    public static index(request,response){
        var params  =   request.body;

        var userExpenseManager         =   UserExpenseManager.getInstance();
        userExpenseManager.fetchUsers(params).then((result:Message)=>{
            response.status(result.getResponseCode());
            response.json(result.toJson());
        }).catch(error=>{
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to fetch users",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });
    }


    //Controller method to authenticate the user.
    public static show(request,response){
        var input_param  =   request.params;

        if(input_param['id'] == undefined || input_param['id'] == null ||
        input_param['id'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"User id provided is not valid.")).toJson());
            response.end();
            return;
        }
        var userExpenseManager         =   UserExpenseManager.getInstance();
        var _id      =   (input_param['id']);

        userExpenseManager.getUserById(_id).then((result:Message)=>{
            response.status(result.getResponseCode());
            response.json(result.toJson());
        }).catch(error=>{
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to find user",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });


    }


    //Method to add user
    public static add(request,response){
        var input_data  =   request.body;

        if(input_data['name'] == undefined || input_data['name'] == null ||
            input_data['name'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Name provided is empty.")).toJson());
            response.end();
            return;
        }

        if(input_data['email'] == undefined || input_data['email'] == null ||
            input_data['email'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Email provided is empty.")).toJson());
            response.end();
            return;
        }

        if(input_data['phone'] == undefined || input_data['phone'] == null ||
            input_data['phone'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Phone provided is empty.")).toJson());
            response.end();
            return;
        }

        if(input_data['amount'] == undefined || input_data['amount'] == null ||
            isNaN(input_data['amount'])){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Amount provided is not valid.")).toJson());
            response.end();
            return;
        }
        
        if(input_data['preferred_type'] == undefined || input_data['preferred_type'] == null ||
            input_data['preferred_type'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Preferred Type provided is empty.")).toJson());
            response.end();
            return;
        }

        var preferred_type          =   input_data['preferred_type'];
        if(preferred_type == 'Group'){
            console.log(preferred_type);
            if(input_data['members'] == undefined || input_data['members'] == null ){
                response.status(400);
                response.json((new Message(Message.INVALID_PARAM,"Members provided is empty.")).toJson());
                response.end();
                return;
            }
        } 
         
        if(input_data['media_id'] == undefined || input_data['media_id'] == null ||
        input_data['media_id'].trim().length == 0){
            response.status(400);
            response.json((new Message(Message.INVALID_PARAM,"Please upload document.")).toJson());
            response.end();
            return;
        }       
         

        
        var userExpenseManager         =   UserExpenseManager.getInstance();
        var name                =   input_data['name'].trim();
        var email               =   input_data['email'].trim();
        var phone               =   input_data['phone'];
        var amount              =   input_data['amount'];
        var members             =   input_data['members'];
        var preferred_type      =   input_data['preferred_type'];
        var media_id            =   input_data['media_id'];

      
        userExpenseManager.add(name,email,phone,amount,preferred_type,members,media_id).then((result:Message)=>{
            response.status(result.getResponseCode());
            response.json(result.toJson());
        }).catch(error=>{
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to add user",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });
    }

  

    

    

}


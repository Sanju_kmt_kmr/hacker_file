/*
 |-------------------------------------------------------------------
 |  User.ts - Module User is use to manage the user of the application
 |-------------------------------------------------------------------
 */

import {Validation} from './Util/Validation';
import {Message} from './Util/Message';
import * as q from 'q';
import * as bcrypt from 'bcrypt';
const saltRounds = 10;
import * as jwt from 'jsonwebtoken';
import * as env from './../../config/env.json';
//Including the Model dependencies;
import * as UserExpense from "./../../database/UserExpense";
import * as mongoose from "mongoose";


export class UserExpenseManager {

    private static _instance:UserExpenseManager  =   null;

    //Private constructor
    private constructor(){
    }

    public static getInstance(){
        if(UserExpenseManager._instance == null){
            UserExpenseManager._instance  =   new UserExpenseManager;
        }
        return UserExpenseManager._instance
    }

    public add(name:String,email:string,phone:String,amount:String,preferred_type:String,members:String,media_id:String){
        var defer       =   q.defer();

        if(email == undefined || email == null || email.trim().length == 0){
            defer.reject((new Message(Message.INVALID_PARAM,"Email address is empty")));
            return defer.promise;
        }

        
        if(name != null && name== undefined && name.trim().length != 0){
            defer.reject((new Message(Message.INVALID_PARAM,"Name is empty")));
            return defer.promise;
        }
        if(media_id != null && media_id== undefined && media_id.trim().length != 0){
            defer.reject((new Message(Message.INVALID_PARAM,"Media id is empty")));
            return defer.promise;
        }

        if(phone != null && phone == undefined && phone.trim().length != 0){
            defer.reject((new Message(Message.INVALID_PARAM,"Phone is empty")));
            return defer.promise;
        }

        if(amount != null && amount == undefined && amount.trim().length != 0){
            defer.reject((new Message(Message.INVALID_PARAM,"amount is empty")));
            return defer.promise;
        }
         
        UserExpense .create({
            email,
            name,
            phone,
            amount,
            preferred_type,
            members,
            media_id,
            'createdAt':(new Date()),
        }).then((result)=>{
            defer.resolve((new Message(Message.SUCCESS,"Expense Added Sucessfully.",result)));
        }).catch((error)=>{
            defer.reject((new Message(Message.INTERNAL_ERROR,"Failed to add ",error)));

        })
        return defer.promise;
    }

    

    public getUserById(user_id:string){
        var defer   =   q.defer();
        UserExpense.findOne(
        {
                '_id':user_id
            }
        ).then((user)=>{
            if(user == null){
                defer.reject(new Message(Message.NOT_FOUND,"User not found id"));
            } else {
                defer.resolve(new Message(Message.SUCCESS,"User found",user));
            }

        }).catch(err=>{
            defer.reject(new Message(Message.INTERNAL_ERROR,"Failed to find user",err));
        });
        return defer.promise;
    }


   

    public fetchUsers(data:any={}){
        var defer   =   q.defer();

        var where_query     =   {};

        if(data['where']  != undefined && data['where'] != null){
            where_query     =   data['where'];
        }

        if(data['search_query'] != undefined && data['search_query'] != null && (data['search_query']+'').length != 0){
            var query_string        =   (data['search_query'] +'').toLowerCase().trim();
            where_query['$or']      =   [
                {
                    'name':{
                        '$regex':'.*'+query_string+'.*',
                        '$options':'i'
                        
                    }
                },
                {
                    'email':{
                        '$regex':'.*'+query_string+'.*',
                        '$options':'i'
                        
                    }
                }
            ]
        }
        var options       = {};
        if(data['order'] != undefined && data['order'] != null){
            options['sort']     =   data['order'];
        }

        var attributes  = ['_id','name', 'email','phone','amount','members','preferred_type','createdAt','updatedAt'];  
       

        if(data['type'] != "all"){
            var page    =   1;
            if(data['page'] != undefined && data['page'] != null && !isNaN(data['page'])){
                page    =   parseInt(data['page']);
            }

            var page_size    =   15;
            if(data['page_size'] != undefined && data['page_size'] != null && !isNaN(data['page_size'])){
                page_size    =   parseInt(data['page_size']);
            }

            var offset          =       (page - 1)*page_size ;

            options['skip']   =   offset;
            options['limit']   =   page_size;
        } 

        var data_count  =   0;
        UserExpense.count(where_query).then((count)=>{
            data_count      =   count;
            return  UserExpense.find(where_query,attributes,options).populate('media_id');
        }).then(users => {
            var result  =   {};
            result['rows']      =   users;
            result['count']      =   data_count;
            result['page']   =   page;
            result['page_size']   =   page_size;
            defer.resolve(new Message(Message.SUCCESS,"List of users",result));
        }).catch(error=>{
            defer.reject((new Message(Message.INTERNAL_ERROR,"Failed to find users.",error)));
        });
        return defer.promise;
    }

}

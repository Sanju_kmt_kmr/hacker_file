/*
 |-------------------------------------------------------------------
 |  User.ts - Module User is use to manage the user of the application
 |-------------------------------------------------------------------
 */

import {Validation} from './Util/Validation';
import {Message} from './Util/Message';
import * as q from 'q';
import * as bcrypt from 'bcrypt';
const saltRounds = 10;
import * as jwt from 'jsonwebtoken';
import * as env from './../../config/env.json';
//Including the Model dependencies;
import * as User from "./../../database/User";
import * as mongoose from "mongoose";


export class UserManager {

    private static _instance:UserManager  =   null;

    //Private constructor
    private constructor(){
    }

    public static getInstance(){
        if(UserManager._instance == null){
            UserManager._instance  =   new UserManager;
        }
        return UserManager._instance
    }

    //Method is used to authenticate the user

    public authenticate(email:string,password:string,remember_me:boolean=false,user_type:number){
        var defer       =   q.defer();

        if(email == null || email.trim().length == 0){
            defer.reject((new Message(Message.INVALID_PARAM,"Email address is empty")));
            return defer.promise;
        }

        if(password == null || password.trim().length <  6){
            defer.reject((new Message(Message.INVALID_PARAM,"Incorrect password for user.")));
            return defer.promise;
        }
        var me  =   this;
        User.findOne(
            {email:email,user_type:user_type}
        ).then(user => {
            if(user == null){
                defer.reject((new Message(Message.NOT_FOUND,"Failed to find user.")));
            } else {
                var user_hashed_pass        =   user.password;
                if(me.checkHash(password,user_hashed_pass)){
                    // var options:any         =   {expiresIn:30};
                    var options:any      =   {expiresIn:"3h"}
                    if(remember_me){
                        options      =   {};
                    }
                    var token = jwt.sign({ user_id: user.id }, env['SECRET'],options);
                    var result  =   {
                        'id':user.id,
                        'token':token
                    };
                    defer.resolve((new Message(Message.SUCCESS,"User successfully authenticated.",result)));
                    user.last_login_at  =   new Date();
                    user.save();
                } else {
                    defer.reject((new Message(Message.AUTH_FAILED,"Incorrect password for user.")));
                }
            }
        }).catch(err=>{
            defer.reject((new Message(Message.INTERNAL_ERROR,"Failed to find user.",err)));
        });

        return defer.promise;
    }

    public authenticateBuddy(email:string,password:string,remember_me:boolean=false,user_type:string){
        var defer       =   q.defer();

        if(email == null || email.trim().length == 0){
            defer.reject((new Message(Message.INVALID_PARAM,"Email address is empty")));
            return defer.promise;
        }

        if(password == null || password.trim().length <  6){
            defer.reject((new Message(Message.INVALID_PARAM,"Incorrect password for user.")));
            return defer.promise;
        }
        var me  =   this;
        var where_query     =   {
            'email' : email, 
            '$or' : [
                {'user_type' : 2 },
                {'user_type' : 3 }
            ]
        };
        console.log(where_query);
        console.log('Checking Buddy Manager');
        User.findOne(
            where_query
        ).then(user => {
            console.log('User Found Now checking password');
            console.log(user);
            if(user == null){
                defer.reject((new Message(Message.NOT_FOUND,"Failed to find user.")));
            } else {
                var user_hashed_pass        =   user.password;
                if(me.checkHash(password,user_hashed_pass)){
                    // var options:any         =   {expiresIn:30};
                    var options:any      =   {expiresIn:"3h"}
                    if(remember_me){
                        options      =   {};
                    }
                    var token = jwt.sign({ user_id: user.id }, env['SECRET'],options);
                    var result  =   {
                        'id'   : user.id,
                        'token': token
                    };
                    defer.resolve((new Message(Message.SUCCESS,"User successfully authenticated.",result)));
                    user.last_login_at  =   new Date();
                    user.save();
                } else {
                    defer.reject((new Message(Message.AUTH_FAILED,"Incorrect password for user.")));
                }
            }
        }).catch(err=>{
            defer.reject((new Message(Message.INTERNAL_ERROR,"Failed to find user.",err)));
        });

        return defer.promise;
    }

    public addUser(data:any={}){
        var defer       =   q.defer();
        var createData = {
            'name'          : '',
            'email'         : '',
            'phone_number'  : '',
            'password'      : '',
            'user_type'     : '',
            'trainer_id'    : null,
            'buddy_id'      : null,
            'status'        : false,
            'createdAt'     :(new Date()),
        }

        if(data['email'] == undefined || data['email'] == null || data['email'].trim().length == 0){
            defer.reject((new Message(Message.INVALID_PARAM,"Email address is empty")));
            return defer.promise;
        }else{
            createData.email = data['email']; 
        }

        if(data['password'] == null || data['password'].trim().length <  6){
            defer.reject((new Message(Message.INVALID_PARAM,"Password["+data['password']+"] should contain minimum of 6 characters.")));
            return defer.promise;
        }else{
            createData.password = this.hash(data['password']);
        }

        if(data['name'] != null && data['name'].trim().length != 0){
            createData.name    =   data['name'];
        }

        if(data['phone_number'] != null && data['phone_number'].trim().length != 0){
            createData.phone_number    =   data['phone_number'];
        }

        if(data['user_type'] != null){
            createData.user_type    =   data['user_type'];
        }

        if(data['trainer_id'] != null){
            createData.trainer_id    =   data['trainer_id'];
        }

        if(data['buddy_id'] != null){
            createData.buddy_id    =   data['buddy_id'];
        }

        User.findOne(
            {
                email : createData.email
            }
        ).then(user => {
            // projects will be an array of all Project instances
            if(user == null){
                User.create(createData).then(function(result){
                    defer.resolve((new Message(Message.SUCCESS,"User Added Sucessfully.",result)));
                }).catch(function(err){
                    defer.reject((new Message(Message.INTERNAL_ERROR,"Failed to add user.",err)));
                });
            } else {
                defer.reject((new Message(Message.ALREADY_EXISTS,"The email which you have entered is already exist, Please use a different email")));
            }
        }).catch(err=>{
            defer.reject((new Message(Message.INTERNAL_ERROR,"Failed to add user.",err)));
        });
        return defer.promise;
    }

    //Method to generate hash for password
    public hash(password:string){
        return bcrypt.hashSync(password, saltRounds);
    }

    //Method to validate the hash
    public checkHash(password:string,hashString:string){
        return bcrypt.compareSync(password, hashString);
    }

    public getUserById(user_id:string){
        var defer   =   q.defer();
        User.findOne(
        {
                '_id':user_id
            }
        ).then((user)=>{
            if(user == null){
                defer.reject(new Message(Message.NOT_FOUND,"User not found id"));
            } else {
                defer.resolve(new Message(Message.SUCCESS,"User found",user));
            }

        }).catch(err=>{
            defer.reject(new Message(Message.INTERNAL_ERROR,"Failed to find user",err));
        });
        return defer.promise;
    }

    public getUserByEmail(email_id:string){
        var defer   =   q.defer();
        User.findOne(
            {
                'email':email_id
            }
        ).then((user)=>{
            if(user == null){
                defer.reject(new Message(Message.NOT_FOUND,"User not found email"));
            } else {
                defer.resolve(new Message(Message.SUCCESS,"User found",user));
            }

        }).catch(err=>{
            defer.reject(new Message(Message.INTERNAL_ERROR,"Failed to find user",err));
        });
        return defer.promise;
    }


    public updateUser(user_id:string,data:any={}){
        var defer   =   q.defer();

        var me      =   this;
        this.getUserById(user_id).then((result:Message)=>{
            if(result.getMessageCode() == Message.SUCCESS){
                var user    =   result.getMessageData();

                if(data['name'] != undefined && data['name'] != null && data['name'].trim().length != 0){
                    user.name   =   data['name'].trim();
                }

                if(data['password'] != undefined && data['password'] != null && data['password'].trim().length != 0){
                    user.password   =   me.hash(data['password']);
                }

                if(data['user_type'] != undefined && data['user_type'] != null && (data['user_type']+'').trim().length != 0 && !isNaN(data['user_type'])){
                    user.user_type   =   parseInt(data['user_type']+'');
                }

                if(data['status'] != undefined && data['status'] != null ){
                    user.status   =   (data['status']);
                }

                if(data['profile_image_path'] != undefined && data['profile_image_path'] != null && data['profile_image_path'].trim().length != 0){
                    user.profile_image_path   =   data['profile_image_path'].trim();
                }

                if(data['trainer_id'] != null){
                    var id   =   data['trainer_id'];
                    user.trainer_id    = mongoose.Types.ObjectId(id);

                }

                user.updatedAt  =   (new Date());
                user.save().then((nuser)=>{
                    defer.resolve(new Message(Message.SUCCESS,"User updated",nuser));
                }).catch(err=>{
                    defer.reject(new Message(Message.INTERNAL_ERROR,"Failed to update user",err));
                });

            } else {
                defer.reject(result);
            }
        },(err:Message)=>{
            defer.reject(err);
        })
        return defer.promise;
    }

   

    public fetchUsers(data:any={}){
        var defer   =   q.defer();

        var where_query     =   {};

        if(data['where']  != undefined && data['where'] != null){
            where_query     =   data['where'];
        }

        if(data['search_query'] != undefined && data['search_query'] != null && (data['search_query']+'').length != 0){
            var query_string        =   (data['search_query'] +'').toLowerCase().trim();
            where_query['$or']      =   [
                {
                    'name':{
                        '$regex':'.*'+query_string+'.*',
                        '$options':'i'
                        
                    }
                },
                {
                    'email':{
                        '$regex':'.*'+query_string+'.*',
                        '$options':'i'
                        
                    }
                }
            ]
        }
        var options       = {};
        if(data['order'] != undefined && data['order'] != null){
            options['sort']     =   data['order'];
        }

        var attributes  = ['_id','name', 'email','user_type','last_login_at','status','createdAt','updatedAt'];  
       

        if(data['type'] != "all"){
            var page    =   1;
            if(data['page'] != undefined && data['page'] != null && !isNaN(data['page'])){
                page    =   parseInt(data['page']);
            }

            var page_size    =   15;
            if(data['page_size'] != undefined && data['page_size'] != null && !isNaN(data['page_size'])){
                page_size    =   parseInt(data['page_size']);
            }

            var offset          =       (page - 1)*page_size ;

            options['skip']   =   offset;
            options['limit']   =   page_size;
        } 

        var data_count  =   0;
        User.count(where_query).then((count)=>{
            data_count      =   count;
            return  User.find(where_query,attributes,options);
        }).then(users => {
            var result  =   {};
            result['rows']      =   users;
            result['count']      =   data_count;
            result['page']   =   page;
            result['page_size']   =   page_size;
            defer.resolve(new Message(Message.SUCCESS,"List of users",result));
        }).catch(error=>{
            defer.reject((new Message(Message.INTERNAL_ERROR,"Failed to find users.",error)));
        });
        return defer.promise;
    }

}

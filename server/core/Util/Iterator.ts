/*
 |-------------------------------------------------------------------
 |  Iterator.ts -    Iterator pattern is used to iterate the promise
 |                  In async manner.
 |-------------------------------------------------------------------
 */

import * as q from "q";
import {Message} from "./Message";
import { isArray } from "util";

export class Iterator{

    private items                            =      [];
    private iterator                         =      null;
    private stepNum                          =      0;

    public constructor(){
    }

    //Setter for Item to Iterate
    public setArrayToIterate(item){
        this.items  =   item;
    }

    //Setter for Item to Iterate
    public setIterator(iterator){
        this.iterator  =   iterator;
    }


    public getCurrentStep(){
        return this.stepNum;
    }

    public start(){
        var defer           =   q.defer();

        if(this.items == null || !isArray(this.items) || this.items.length ==0){
            defer.reject(new Message(
                Message.INVALID_PARAM,"Items array is empty"
            ));
            return defer.promise;
        }

        if(this.iterator == null){
            defer.reject(new Message(
                Message.INVALID_PARAM,"Iterator is not defined"
            ));
            return defer.promise;
        }

        this.stepNum    =   0;

        var me      =   this;
        var result  =   [];
        function checker(error,data){
            if(error != null){
                defer.reject(new Message(Message.INTERNAL_ERROR,"Iterator failed at step "+me.stepNum,error));
            }
            if(error == null){
                result.push(data);
            }
            
            me.stepNum++;
            if(me.stepNum  >=  me.items.length){
                if(error == null){
                    defer.resolve(new Message(Message.SUCCESS,"Iterator completed",result));
                } else {
                    defer.reject(new Message(Message.INTERNAL_ERROR,"Iterator failed at step "+(me.stepNum-1),error));
                }
            } else {
                
                me.iterator(me.items[me.stepNum],checker);
            }
        }
  
        this.iterator(this.items[0], checker);
        return defer.promise;
    }
    
}
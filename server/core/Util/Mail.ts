/*
 |-------------------------------------------------------------------
 |  Mail.ts - module is used to send Email to user with DOT Template
 |-------------------------------------------------------------------
 */

import * as nodemailer from 'nodemailer';
import * as env from '../../../config/env.json';
import { Validation } from './Validation';
import * as q from 'q';
import { Message } from './Message';

export class Mail {

    private html    =   null;
    private from            =   null;
    private to              =   [];
    private subject         =   null;
    private attachments         =   [];
    private transporter     =   null;

    private validator:Validation = null;
    //Public constructor
    public constructor(transporter=null){

        this.validator      =   Validation.getInstance();

        if(transporter == null){
            if(env['EMAIL_TRANPORTER'] != undefined){
                this.transporter  =   nodemailer.createTransport(env['EMAIL_TRANPORTER'])
            } else {
                throw new Error("Transporter not defined in env.");
            }
        } else {
            try{
                this.transporter  =   nodemailer.createTransport(transporter)
            } catch(e){
                throw new Error("Invalid email transport configuration provided");
            }
        }
    }

    public setHTML(html){
        this.html   =   html;
    }

    public getHTML(){
        return this.html;
    }

    public setFrom(from){
        if(!this.validator.validateEmail(from.email)){
            throw new Error("Invalid from email id");
        }
        this.from   =   from;
    }

    public getFrom(){
        return this.from;
    }

    public addTo(to){
        if(!this.validator.validateEmail(to.email)){
            throw new Error("Invalid to email id.");
        }
        this.to.push(to);
    }

    public getTo(){
        return this.to;
    }

    public setSubject(subject){
        this.subject    = subject;
    }

    public getSubject(){
        return this.subject;
    }

    public addAttachment(file_name,path,cid){
        this.attachments.push({
            filename:file_name,
            path:path,
            cid:cid
        });
    }

    //Static methos is used to send mail to user
    public  send(){
        var defer   =   q.defer();

        var from    =   "";

        if(this.from == null){
            this.from   =   {
                'email':env['EMAIL_TRANPORTER']['auth']['user']
            }
        }
        
        if(this.from.name == undefined){
            from    =   this.from.email;
        } else {
            from    =   '"'+this.from.name+'" <'+this.from.email+'>';
        }
        
        if(this.to == null || this.to.length == 0){
            defer.reject(new Message(Message.INVALID_PARAM,"to address not provided"));
            return defer.promise;
        }

        var to       =  [];
        this.to.forEach((recipent)=>{
            var temp_tp     =   "";
            if(recipent.name == undefined){
                temp_tp    =   recipent.email;
            } else {
                temp_tp    =   '"'+recipent.name+'" <'+recipent.email+'>';
            }

            to.push(temp_tp);
        })
        let mailOptions = {
            from: from, // sender address
            to: to,
            subject: this.subject,
            html: this.html,
        };

        if(this.attachments.length > 0){
            mailOptions['attachments']  =   this.attachments;
        }

        this.transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                defer.reject(error);
                return console.log(error);
            }
            defer.resolve(new Message(Message.SUCCESS,"Mail Sent"));
            console.log('Message sent: %s', info.messageId);
        });

        return defer.promise;
    }
}
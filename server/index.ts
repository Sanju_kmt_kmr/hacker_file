/*
 |-----------------------------------------------------------------
 |  index.ts    - Start point of application.
 |-----------------------------------------------------------------
 |
 */

 //Including the typing
/// <reference path="typing.d.ts" />


//Including the dependencies for server
import {Server} from './server';
import * as env from './../config/env.json';

var server:Server;

process.env.TZ = env['TIMEZONE'];

try{
    //Setting up the server
    server      =   new     Server(env['ENV'],env['PORT']);

    //Running Server
    server.run();
} catch(e){
    console.log(e);
    process.exit(0);
}


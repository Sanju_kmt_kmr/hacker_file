/*
 |-----------------------------------------------------------------------------
 |  TokenMiddleware.js  - Token middleware handle request and validate whether 
 |                        valid token is present with request or not
 |-----------------------------------------------------------------------------
 */
import {Message} from '../core/Util/Message';
import {UserManager} from '../core/UserManager';
import * as env from './../../config/env.json';
import * as jwt from "jsonwebtoken";

export default function(request,response,next){
    var headers     =   request.headers;

    if(headers.authorization == undefined){
        var message         =    new Message(
            Message.INVALID_HEADER,
            "Authoration Header not passed"
        );
        response.status(message.getResponseCode());
        response.json(message.toJson());
        return;
    }

    var authorization_header    =   headers.authorization;
    var check_head_star         =   authorization_header.substr(0,6);
    if(check_head_star != 'Bearer'){
        var message         =   new Message(
            Message.INVALID_HEADER,
            "Invalid Authoration Header"
        );
        response.status(message.getResponseCode());
        response.json(message.toJson());

    }
    var token           =   authorization_header.substr(7);

    // verify a token symmetric
    jwt.verify(token, env['SECRET'], function(error, decoded) {
        if(error != null){
            var err     =   new Message(Message.INVALID_PARAM,"Invalid token",error);
            response.status(err.getResponseCode());
            response.json(err.toJson());
            return;
        }
        
        var userManager     =   UserManager.getInstance();
        userManager.getUserById(decoded.user_id).then((result:Message)=>{
            request['auth_user']    =   result.getMessageData();
            next();
        }).catch(error=>{
            console.log(error);
            if(error instanceof Message){
                response.status(error.getResponseCode());
                response.json(error.toJson());
            } else {
                var message     = new Message(Message.INTERNAL_ERROR,"Failed to validate token",error);
                response.status(message.getResponseCode());
                response.json(message.toJson());
            }
        });
    });
}
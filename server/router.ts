/*
 |-------------------------------------------------------------------
 |  router.ts - Defination of rountes goes here
 |-------------------------------------------------------------------
 */

import * as Controller from './controllers/index';
import {default as AuthMiddleware} from './middleware/AuthMiddleware';

export class Router{

    private application;

    //Constructor For applicatin
    constructor(app){
        this.application    =   app;
    }

    //Association of rounters goes here
    public associate(){
        //Defination of routes will go here
        this.application.get('/',Controller.AppViewController.index);
        this.application.get('/admin-pannel',Controller.AdminViewController.index);

        //Api section started
        this.application.post('/api/user/authenticate',Controller.UserController.authenticate);
        this.application.get('/api/user/me',AuthMiddleware,Controller.UserController.me);
        this.application.post('/api/user/add',AuthMiddleware,Controller.UserController.add);
        this.application.get('/api/user/:id',AuthMiddleware,Controller.UserController.show);
        this.application.post('/api/user',AuthMiddleware,Controller.UserController.index);

        this.application.get('/media',Controller.MediaController.show);
        this.application.get('/api/media/:id',Controller.MediaController.showById);
        this.application.post('/api/media/add',AuthMiddleware,Controller.MediaController.add);

        this.application.post('/api/user_expense',AuthMiddleware,Controller.UserExpenseController.index);
        this.application.post('/api/user_expense/add',AuthMiddleware,Controller.UserExpenseController.add);


    }
}

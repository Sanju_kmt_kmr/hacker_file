/*
 |------------------------------------------------------------------------------------
 |  admin_init.ts - Admin init initialization script.
 |------------------------------------------------------------------------------------
 |
 |
 |
 */
/// <reference path="../typing.d.ts" />

 import {UserManager} from './../core/UserManager';
 import * as env from './../../config/env.json';
 import {Message} from './../core/Util/Message';

 var userManager     =   UserManager.getInstance();

 var data = {
     'email'        : 'admin@gmail.com',
     'password'     : 'password',
     'user_type'    : 1,
     'name'         : 'Admin'
 };

 userManager.addUser(data).then((message:Message)=>{
    process.exit(0);
 },(message:Message)=>{
    var messageData     =   message.getMessageData();
    if(messageData != null){
        console.log(messageData);
    }
    process.exit(1);
 });

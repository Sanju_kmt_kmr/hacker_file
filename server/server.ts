/*
 |-------------------------------------------------------------------
 |  server.ts - File for server defination
 |-------------------------------------------------------------------
 |  File contains the class to manage the server. It consist of logic
 |  for bootstraping of server and Middleware initization and biniding
 |  with server port.
 |
 */
import * as express from 'express';
import {Router}     from './router';
import * as bodyparser from 'body-parser';
import * as fileUpload from 'express-fileupload';
import * as path  from 'path';

import * as dot from "dot";
var renderer  =  require('./../render');


export class Server{

    private env:string;
    private port:string;
    
    // private server: Server;
    // private io: SocketIO.Server;

    private app     =   null;
    private router  =   null;
    private server  =   null
    private socketEvents    =   null;

    //Constructor For applicatin
    constructor(env:string,port:string){
        if(env == null || env == undefined || env.trim().length == 0){
            throw new Error('Empty/Null/Undefined eviorment provided.');
        }

        if(port == null || port == undefined){
            throw new Error('Port not provided.');
        }

        this.env        =   env;
        this.port       =   port;
        this.bootstarp();
    }

    //Bootstarping the server
    private bootstarp():void{
        //Initializing the express
        this.app    =   express();

        this.server = require('http').Server(this.app);

        dot.templateSettings = {
            evaluate:    /\[\[([\s\S]+?)\]\]/g,
            interpolate: /\[\[=([\s\S]+?)\]\]/g,
            encode:      /\[\[!([\s\S]+?)\]\]/g,
            use:         /\[\[#([\s\S]+?)\]\]/g,
            define:      /\[\[##\s*([\w\.$]+)\s*(\:|=)([\s\S]+?)#\]\]/g,
            conditional: /\[\[\?(\?)?\s*([\s\S]*?)\s*\]\]/g,
            iterate:     /\[\[~\s*(?:\]\]|([\s\S]+?)\s*\:\s*([\w$]+)\s*(?:\:\s*([\w$]+))?\s*\]\])/g,
            varname: 'it',
            strip: true,
            append: true,
            selfcontained: false
        };
        
        dot.process({ global: "_page.render"
             ,destination: __dirname + "/../render"
             ,path: (__dirname + "/../views")
        });

        // parse application/x-www-form-urlencoded
        this.app.use(bodyparser.urlencoded({ extended: false }));
        // parse application/json
        this.app.use(bodyparser.json());

        // console.log(),"LOKEHS");
        this.app.use(express.static(path.join(__dirname,"../public")));

        this.app.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods","GET, POST, PUT, DELETE, OPTION, HEAD");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Accept-Language");
            next();
        });

        this.app.use(fileUpload());


        this.router     =   new Router(this.app);

        //Associating the routers to application
        this.router.associate();
    }

    //Running the server
    public run():void{
        var me      =   this;
        if(this.server  == null){
            throw new Error('Express application is not initialized.');
        }
        
        this.server.listen(this.port,function(){
            console.log('Application started on port '+me.port);
        });
           
    }
}
